## [6.1.13] - 2018-03-02
## Fixed
 - Fixed view code when view starts with an error
 - Fixed double start sent in some cases
 - Fixed wrong jointime when using plugin init
 - Viewcode changed for frozen views
 ## Added
 - Cancel buffer method
 - Cancel seek method
 - getHouseshold method in adapter
 - parse.locationHeader option

## [6.1.12] - 2018-02-13
## Fixed
 - Fixed pauseDuration not being sent in stop
 - Adapter fireInit behaviour with auto start
 - Prevent crash if storage is not available
## Added
 - cdnType and cdnNode can be set as an option
 - fireCasted method for adapter

## [6.1.11] - 2018-01-30
## Added
 - Buffer monitor for custom playrates

## [6.1.10] - 2018-01-29
## Added
 - IsP2PEnabled for peer5
 - Added 10 ad extraparam
 - Reported playhead for live content


## [6.1.9] - 2018-01-16
## Added
 - Added userType and content.streamingProtocol options
 ## Fixed
 - Changed p2p params names

## [6.1.8] - 2017-12-22
## Fixed
 - Fixed access to storages when is not possible but exists
 - Added ad.campaign, ad.title and ad.resource options
 - Removed background detection

## [6.2.0-beta2] - 2017-12-18
## Added
 - Request number param to prevent /data calls being cached
 - P2PEnabled param activated

## [6.2.0-beta] - 2017-12-05
## Added
 - Device detection (android, iphone, desktop)
 - Background detection and actions
 - Options for background detection 'autoDetectBackground' and 'autoDetectBackgroundSettings'

## [6.1.7] - 2017-12-04
## Fixed
 - Now error message is 'msg' instead of 'errorMsg'

## [6.1.6] - 2017-11-28
## Fixed
 - FatalError now sends error and stop
 - Stop event sends pauseDuration if needed

## [6.1.5] - 2017-11-23
## Added
 - P2P enabled parameter getter
 - Option obfuscateIP

## [6.1.4] - 2017-11-14
### Fixed
 - Semicolons no longer ignored in hlsparser
 - IsLive detection when start
 - resource.js init fix
 - Preventing to send init twice
## Added
 - Automatic use of adInit when needed

## [6.1.3] - 2017-10-31
### Fixed
 - Several issues with init, start and jointime events

## [6.1.0] - 2017-10-23
### Added
- Support for Streamroot and peer5 p2p metrics
- Send player name on start
- Send media duration and resource on jointime
- Get throughput values by default when using p2p
- Remove playhead and media duration of live content

## [6.0.10] - 2017-10-16
### Added
- FireStop with end param to end views with ad.afterStop option

## [6.0.9] - 2017-10-10
### Added
- Option ad.afterStop to catch ads after player stop

## [6.0.8] - 2017-10-04
### Added
- Option ad.ignore to block AdsAdapter messages

## [6.0.7] - 2017-10-02
### Added
- Add playhead on ad events

## [6.0.6] - 2017-09-21
### Added
- Add p2p metrics

## [6.0.5] - 2017-07-24
### Added
- Add init and adInit events, called with `fireInit`
- Add `fireNoServedAd` as special error

## [6.0.4] - 2017-07-24
### Added
- add `isDone` property to `ResourceTransform`.
- Add more `extraParam`, up to 20.

## [6.0.3] - 2017-06-06
### Changed
- Removed `manifest.json` from repo.
- Removed `manifest.json` from `.npmignore`.

## [6.0.2] - 2017-06-06
### Added
- `username` added to infinity's `nav`.

### Git
- Added `manifest.json` to `.gitignore`.

## [6.0.1] - 2017-06-01
### Fixed
- Fix several `infinity` module issues.

### Added
- Support for `adError`, `adClick` and `adBlocked`.
- Add `getComm` to `Plugin` and `Infinity`.
- Add `npm run clean` command to delete `dist`, `coverage` and `deploy` folders.

## [6.0.0] - 2017-05-30
### Fixed
- Fixed CDN Parser issues
- Fixed /error before /init

## [6.0.0-rc.infinity] - 2017-05-19
### Added
- Infinity module

## [6.0.0-rc] - 2017-01-09
### Added
- First release
