describe('YInfinity', () => {
  var YInfinity = require('../../../src/infinity/infinity')
  var pluginMock = {
    viewTransform: {},
    getContext: () => { },
    storage: {
      setSession: () => { }
    }
  }
  var infinity

  beforeEach(() => {
    infinity = new YInfinity(pluginMock)
    global.btoa = () => { }
    global.btoa = spyOn(global, 'btoa')
  })

  it('return comm', () => {
    expect(infinity.getComm()).toBeUndefined()
    infinity.begin()
    expect(infinity.getComm()).toBeDefined()
    expect(global.btoa).toHaveBeenCalled()
  })

  it('should sessionStart', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })
    infinity.fireSessionStart()
  })

  it('should sessionStop', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_STOP, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_STOP)
      done()
    })
    infinity.fireSessionStop()
  })

  it('should nav', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.NAV, (e) => {
      expect(e.type).toBe(YInfinity.Event.NAV)
      done()
    })
    infinity.fireNav()
  })

  it('should event', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.EVENT, (e) => {
      expect(e.type).toBe(YInfinity.Event.EVENT)
      done()
    })
    infinity.fireEvent()
  })

  it('should start at begin', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })

    infinity.begin()
  })

  it('should start at andBeyond', (done) => {
    var infinity = new YInfinity(pluginMock)
    infinity.on(YInfinity.Event.SESSION_START, (e) => {
      expect(e.type).toBe(YInfinity.Event.SESSION_START)
      done()
    })

    infinity.andBeyond()
  })
})
