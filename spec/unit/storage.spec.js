describe('Storage', () => {
  var YStorage = require('../../src/plugin/storage')
  var Log = require('../../src/log')
  var storage

  beforeEach(() => {
    storage = new YStorage()
    spyOn(Log, 'report')
  })

  it('should localstorage', () => {
    storage.setLocal('a', 'c')
    expect(storage.getLocal('a')).toBe('c')
  })

  it('should sessionstorage', () => {
    storage.setSession('b', 'd')
    expect(storage.getSession('b')).toBe('d')
  })

  it('should not fail if storage does not exist', () => {
    global.sessionStorage = global.localStorage = undefined
    storage.setLocal()
    storage.getLocal()
    storage.setSession()
    storage.getSession()
    expect(Log.report).toHaveBeenCalled()
  })
})
