describe('Plugin', () => {
  var Plugin = require('../../../src/plugin/plugin')
  var plugin
  var adapter = {
    id: 'adapter',
    on: function () { },
    off: function () { },
    dispose: function () { }
  }

  beforeEach(() => {
    plugin = new Plugin()
  })

  it('should declare options', () => {
    plugin.setOptions({ accountCode: 'a' })
    expect(plugin.options.accountCode).toBe('a')
  })

  it('should add and remove adapters', () => {
    spyOn(adapter, 'on')
    spyOn(adapter, 'off')
    spyOn(adapter, 'dispose')
    adapter.plugin = null
    plugin.setAdapter(adapter)
    expect(adapter.on).toHaveBeenCalled()
    expect(plugin.getAdapter().id).toBe('adapter')
    plugin.removeAdapter()
    expect(adapter.off).toHaveBeenCalled()
    expect(adapter.dispose).toHaveBeenCalled()
    expect(plugin.getAdapter()).toBe(null)
  })

  it('should not add adapter to more than 1 plugin', () => {
    var plugin2 = new Plugin()

    adapter.plugin = null
    plugin.setAdapter(adapter)
    plugin2.setAdapter(adapter)
    expect(plugin2.getAdapter()).toBe(null)

    adapter.plugin = null
    plugin.setAdsAdapter(adapter)
    plugin2.setAdsAdapter(adapter)
    expect(plugin2.getAdsAdapter()).toBe(null)
  })

  it('should add and remove Ads Adapters', () => {
    spyOn(adapter, 'on')
    spyOn(adapter, 'off')
    spyOn(adapter, 'dispose')
    adapter.plugin = null
    plugin.setAdsAdapter(adapter)
    expect(adapter.on).toHaveBeenCalled()
    expect(plugin.getAdsAdapter().id).toBe('adapter')
    plugin.removeAdsAdapter()
    expect(adapter.off).toHaveBeenCalled()
    expect(adapter.dispose).toHaveBeenCalled()
    expect(plugin.getAdsAdapter()).toBe(null)
  })

  it('should disable/enable', () => {
    plugin.disable()
    expect(plugin.options.enabled).toBe(false)
    plugin.enable()
    expect(plugin.options.enabled).toBe(true)
  })

  it('should preload', () => {
    plugin.firePreloadBegin()
    expect(plugin.isPreloading).toBe(true)
    expect(plugin.preloadChrono.startTime).not.toBe(0)
    plugin.firePreloadEnd()
    expect(plugin.isPreloading).toBe(false)
    expect(plugin.getPreloadDuration()).not.toBe(-1)
  })

  it('should reset', () => {
    plugin.firePreloadBegin()
    plugin._reset()
    expect(plugin.isPreloading).toBe(false)
  })

  it('should init', () => {
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(true)
    expect(plugin.viewTransform.nextView).toHaveBeenCalled()
  })

  it('should not init, with adapter inited', () => {
    plugin.getAdapter = function () {
      return {
        flags: {
          isInited: true,
          isStarted: false,
        }
      }
    }
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(false)
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should not init, with adapter started', () => {
    plugin.getAdapter = function () {
      return {
        flags: {
          isInited: false,
          isStarted: true,
        }
      }
    }
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(false)
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should not init if already inited', () => {
    plugin.isInitiated = true
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(true)
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should return comm', () => {
    expect(plugin.getComm()).toBeUndefined()
    plugin._initComm()
    expect(plugin.getComm()).toBeDefined()
  })

  it('should send fatal error', () => {
    spyOn(plugin, 'fireError')
    spyOn(plugin, 'fireStop')
    plugin.fireFatalError()
    expect(plugin.fireError).toHaveBeenCalled()
    expect(plugin.fireStop).toHaveBeenCalled()
  })

  it('should send error with new view', () => {
    spyOn(plugin, '_send')
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireError()
    expect(plugin._send).toHaveBeenCalled()
    expect(plugin.viewTransform.nextView).toHaveBeenCalled()
  })

  it('should send error with previous view', () => {
    plugin.isInitiated = true
    spyOn(plugin, '_send')
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireError()
    expect(plugin._send).toHaveBeenCalled()
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should send error and create new view', () => {
    spyOn(plugin, '_send')
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireError({ errorLevel: 'fatal' })
    expect(plugin._send).toHaveBeenCalled()
    expect(plugin.viewTransform.nextView).toHaveBeenCalled()
  })
})
