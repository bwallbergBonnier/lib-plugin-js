describe('Plugin-info', () => {
  var PlaybackChronos = require('../../../src/adapter/playbackchronos')
  var Plugin = require('../../../src/plugin/plugin')

  var adapter = {
    getPlayhead: function () { return 1 },
    getPlayrate: function () { return 2 },
    getFramesPerSecond: function () { return 3 },
    getDroppedFrames: function () { return 4 },
    getDuration: function () { return 5 },
    getBitrate: function () { return 6 },
    getThroughput: function () { return 7 },
    getRendition: function () { return 'a' },
    getTitle: function () { return 'b' },
    getTitle2: function () { return 'c' },
    getIsLive: function () { return true },
    getResource: function () { return 'd' },
    getPlayerVersion: function () { return 'e' },
    getPlayerName: function () { return 'f' },
    getVersion: function () { return 'g' },
    getPosition: function () { return 'pre' },
    chronos: new PlaybackChronos(),
    on: function () { }
  }

  it('should import from adapter', () => {
    var options = {
      'accountCode': 'nicetest',
      'username': null,
      'network.ip': null,
      'network.isp': null,
      'network.connectionType': null,
      'device.code': null,
      'content.resource': null,
      'content.isLive': null,
      'content.title': null,
      'content.title2': null,
      'content.duration': null,
      'content.transactionCode': null,
      'content.bitrate': null,
      'content.throughput': null,
      'content.rendition': null,
      'content.cdn': null,
      'content.metadata': {},
      'ad.metadata': {},
      'extraparam.1': null,
      'extraparam.2': null,
      'extraparam.3': null,
      'extraparam.4': null,
      'extraparam.5': null,
      'extraparam.6': null,
      'extraparam.7': null,
      'extraparam.8': null,
      'extraparam.9': null,
      'extraparam.10': null
    }

    var plugin = new Plugin(options)
    adapter.plugin = null
    plugin.setAdapter(adapter)
    adapter.plugin = null
    plugin.setAdsAdapter(adapter)

    expect(plugin.getPlayhead()).toBe(1)
    expect(plugin.getPlayrate()).toBe(2)
    expect(plugin.getFramesPerSecond()).toBe(3)
    expect(plugin.getDroppedFrames()).toBe(4)
    expect(plugin.getDuration()).toBe(5)
    expect(plugin.getBitrate()).toBe(6)
    expect(plugin.getThroughput()).toBe(7)
    expect(plugin.getRendition()).toBe('a')
    expect(plugin.getTitle()).toBe('b')
    expect(plugin.getTitle2()).toBe('c')
    expect(plugin.getIsLive()).toBe(true)
    expect(plugin.getResource()).toBe('d')
    expect(plugin.getOriginalResource()).toBe('d')
    expect(plugin.getTransactionCode()).toBe(null)
    expect(plugin.getMetadata()).toBeDefined()
    expect(plugin.getPlayerVersion()).toBe('e')
    expect(plugin.getPlayerName()).toBe('f')
    expect(plugin.getCdn()).toBe(null)
    expect(plugin.getPluginVersion()).toBe('g')
    expect(plugin.getExtraparam1()).toBe(null)
    expect(plugin.getExtraparam2()).toBe(null)
    expect(plugin.getExtraparam3()).toBe(null)
    expect(plugin.getExtraparam4()).toBe(null)
    expect(plugin.getExtraparam5()).toBe(null)
    expect(plugin.getExtraparam6()).toBe(null)
    expect(plugin.getExtraparam7()).toBe(null)
    expect(plugin.getExtraparam8()).toBe(null)
    expect(plugin.getExtraparam9()).toBe(null)
    expect(plugin.getExtraparam10()).toBe(null)
    expect(plugin.getAdPosition()).toBe('pre')
    expect(plugin.getAdPlayhead()).toBe(1)
    expect(plugin.getAdDuration()).toBe(5)
    expect(plugin.getAdBitrate()).toBe(6)
    expect(plugin.getAdTitle()).toBe('b')
    expect(plugin.getAdResource()).toBe('d')
    expect(plugin.getAdMetadata()).toBeDefined()
    expect(plugin.getPluginInfo()).toBeDefined()
    expect(plugin.getIsp()).toBe(null)
    expect(plugin.getConnectionType()).toBe(null)
    expect(plugin.getIp()).toBe(null)
    expect(plugin.getDeviceCode()).toBe(null)
    expect(plugin.getAccountCode()).toBe('nicetest')
    expect(plugin.getUsername()).toBe(null)
    expect(plugin.getNodeHost()).toBe(null)
    expect(plugin.getNodeType()).toBe(null)
    expect(plugin.getNodeTypeString()).toBe(null)
  })

  it('should import from options', () => {
    var options = {
      'accountCode': 'b',
      'username': 'c',
      'parse.hls': true,
      'parse.cdnNode': true,
      'parse.cdnNameHeader': 'aa',
      'parse.cdnNode.list': ['bb'],
      'network.ip': 'f',
      'network.isp': 'g',
      'network.connectionType': 'h',
      'device.code': 'i',
      'content.resource': 'j',
      'content.isLive': false,
      'content.title': 'k',
      'content.title2': 'l',
      'content.duration': 1,
      'content.transactionCode': 'm',
      'content.bitrate': 2,
      'content.throughput': 3,
      'content.fps': 30,
      'content.rendition': 'n',
      'content.cdn': 'o',
      'content.metadata': { p: 'q' },
      'ad.metadata': { r: 's' },
      'extraparam.1': 't',
      'extraparam.2': 'u',
      'extraparam.3': 'v',
      'extraparam.4': 'w',
      'extraparam.5': 'x',
      'extraparam.6': 'y',
      'extraparam.7': 'z',
      'extraparam.8': 'aa',
      'extraparam.9': 'ab',
      'extraparam.10': 'ac'
    }

    var plugin = new Plugin(options)
    adapter.plugin = null
    plugin.setAdapter(adapter)
    adapter.plugin = null
    plugin.setAdsAdapter(adapter)

    expect(plugin.isParseHls()).toBe(true)
    expect(plugin.isParseCdnNode()).toBe(true)
    expect(plugin.getParseCdnNodeList()[0]).toBe('bb')
    expect(plugin.getParseCdnNodeNameHeader()).toBe('aa')
    expect(plugin.getPlayhead()).toBe(1)
    expect(plugin.getPlayrate()).toBe(2)
    expect(plugin.getFramesPerSecond()).toBe(30)
    expect(plugin.getDroppedFrames()).toBe(4)
    expect(plugin.getDuration()).toBe(1)
    expect(plugin.getBitrate()).toBe(2)
    expect(plugin.getThroughput()).toBe(3)
    expect(plugin.getRendition()).toBe('n')
    expect(plugin.getTitle()).toBe('k')
    expect(plugin.getTitle2()).toBe('l')
    expect(plugin.getIsLive()).toBe(false)
    expect(plugin.getResource()).toBe('j')
    expect(plugin.getOriginalResource()).toBe('j')
    expect(plugin.getTransactionCode()).toBe('m')
    expect(plugin.getMetadata().p).toBe('q')
    expect(plugin.getPlayerVersion()).toBe('e')
    expect(plugin.getPlayerName()).toBe('f')
    expect(plugin.getCdn()).toBe('o')
    expect(plugin.getPluginVersion()).toBe('g')
    expect(plugin.getExtraparam1()).toBe('t')
    expect(plugin.getExtraparam2()).toBe('u')
    expect(plugin.getExtraparam3()).toBe('v')
    expect(plugin.getExtraparam4()).toBe('w')
    expect(plugin.getExtraparam5()).toBe('x')
    expect(plugin.getExtraparam6()).toBe('y')
    expect(plugin.getExtraparam7()).toBe('z')
    expect(plugin.getExtraparam8()).toBe('aa')
    expect(plugin.getExtraparam9()).toBe('ab')
    expect(plugin.getExtraparam10()).toBe('ac')
    expect(plugin.getAdPosition()).toBe('pre')
    expect(plugin.getAdPlayhead()).toBe(1)
    expect(plugin.getAdDuration()).toBe(5)
    expect(plugin.getAdBitrate()).toBe(6)
    expect(plugin.getAdTitle()).toBe('b')
    expect(plugin.getAdResource()).toBe('d')
    expect(plugin.getAdPlayerVersion()).toBe('e')
    expect(plugin.getAdMetadata().r).toBe('s')
    expect(plugin.getPluginInfo()).toBeDefined()
    expect(plugin.getIsp()).toBe('g')
    expect(plugin.getConnectionType()).toBe('h')
    expect(plugin.getIp()).toBe('f')
    expect(plugin.getDeviceCode()).toBe('i')
    expect(plugin.getAccountCode()).toBe('b')
    expect(plugin.getUsername()).toBe('c')
    expect(plugin.getNodeHost()).toBe(null)
    expect(plugin.getNodeType()).toBe(null)
    expect(plugin.getNodeTypeString()).toBe(null)
    expect(plugin.getReferer()).toBe('about:blank') // jasmine stuff
  })

  it('should work with crhonos', () => {
    var options = {}
    var plugin = new Plugin(options)
    adapter.plugin = null
    plugin.setAdapter(adapter)
    adapter.plugin = null
    plugin.setAdsAdapter(adapter)

    expect(plugin.getPreloadDuration()).toBe(-1)
    expect(plugin.getJoinDuration()).toBe(-1)
    expect(plugin.getPauseDuration()).toBe(-1)
    expect(plugin.getBufferDuration()).toBe(-1)
    expect(plugin.getSeekDuration()).toBe(-1)
    expect(plugin.getAdJoinDuration()).toBe(-1)
    expect(plugin.getAdPauseDuration()).toBe(-1)
    expect(plugin.getAdBufferDuration()).toBe(-1)
    expect(plugin.getAdTotalDuration()).toBe(-1)
  })

  it('should expose infinity values', () => {
    var plugin = new Plugin()

    plugin.storage.setLocal('session', 1)
    plugin.storage.setSession('context', 2)
    plugin.storage.setSession('data', 3)
    global.document.title = 'a'

    expect(plugin.getSessionId()).toBe(1)
    expect(plugin.getContext()).toBe(2)
    expect(plugin.getStoredData()).toBe(3)
    expect(plugin.getPageName()).toBe('a')

    expect(plugin.getIsSessionExpired()).toBe(true)
    expect(plugin.getIsDataExpired()).toBe(true)
  })
})
