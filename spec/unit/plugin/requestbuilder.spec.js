describe('RequestBuilder', () => {
  var RequestBuilder = require('../../../src/plugin/requestbuilder')
  var cb = function () { return 1 }
  var plugin = {
    getPlayhead: cb,
    getPlayrate: cb,
    getFramesPerSecond: cb,
    getDroppedFrames: cb,
    getDuration: cb,
    getBitrate: cb,
    getThroughput: cb,
    getRendition: cb,
    getTitle: cb,
    getTitle2: cb,
    getIsLive: cb,
    getResource: cb,
    getTransactionCode: cb,
    getMetadata: cb,
    getPlayerVersion: cb,
    getPlayerName: cb,
    getCdn: cb,
    getPluginVersion: cb,
    getExtraparam1: cb,
    getExtraparam2: cb,
    getExtraparam3: cb,
    getExtraparam4: cb,
    getExtraparam5: cb,
    getExtraparam6: cb,
    getExtraparam7: cb,
    getExtraparam8: cb,
    getExtraparam9: cb,
    getExtraparam10: cb,
    getAdPosition: cb,
    getAdPlayhead: cb,
    getAdDuration: cb,
    getAdBitrate: cb,
    getAdTitle: cb,
    getAdResource: cb,
    getAdPlayerVersion: cb,
    getAdMetadata: cb,
    getAdnalyzerVersion: cb,
    getPluginInfo: cb,
    getIsp: cb,
    getConnectionType: cb,
    getIp: cb,
    getDeviceCode: cb,
    getAccountCode: cb,
    getUsername: cb,
    getJoinDuration: cb,
    getBufferDuration: cb,
    getPauseDuration: cb,
    getSeekDuration: cb,
    getAdJoinDuration: cb,
    getAdPauseDuration: cb,
    getAdBufferDuration: cb,
    getTotalAdDuration: cb,
    getPreloadDuration: cb,
    getReferer: cb,
    getNodeHost: cb,
    getNodeType: cb,
    getNodeTypeString: cb,
    getAdapter: function () { return {} },
    getAdsAdapter: function () { return {} }
  }
  var builder

  beforeEach(() => {
    builder = new RequestBuilder(plugin)
  })

  it('should compose params', () => {
    for (var key in RequestBuilder.params) {
      var params = builder.buildParams({}, key)
      for (var key2 in params) {
        expect(params[key2]).toBe(1)
      }
    }
  })

  it('should compose entities', () => {
    var entities = builder.getChangedEntities()
    expect(entities).toBeDefined()
  })

  it('should get adNumbers', () => {
    builder.lastSent.adPosition = 1
    expect(builder.getNewAdNumber()).toBe(1)
    expect(builder.getNewAdNumber()).toBe(2)
    builder.lastSent.adPosition = 2
    expect(builder.getNewAdNumber()).toBe(1)
  })
})
