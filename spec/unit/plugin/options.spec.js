describe('Options', () => {
  var Options = require('../../../src/plugin/options.js')

  it('override options', () => {
    var options = new Options({
      'enabled': false,
      'httpSecure': true,
      'host': 'a',
      'accountCode': 'b',
      'username': 'c',
      'obfuscateIp': false,
      'userType': 'ad',
      'parse.hls': true,
      'parse.cdnNameHeader': 'd',
      'parse.cdnNode': true,
      'parse.cdnNode.list': ['e'],
      'parse.locationHeader': false,
      'network.ip': 'f',
      'network.isp': 'g',
      'network.connectionType': 'h',
      'device.code': 'i',
      'content.resource': 'j',
      'content.isLive': true,
      'content.title': 'k',
      'content.title2': 'l',
      'content.duration': 1,
      'content.transactionCode': 'm',
      'content.bitrate': 2,
      'content.throughput': 3,
      'content.rendition': 'n',
      'content.cdn': 'o',
      'content.cdnNode': 'ae',
      'content.fps': 'af',
      'content.cdnType': 1,
      'content.streamingProtocol': 'HLS',
      'content.metadata': { p: 'q' },
      'ad.metadata': { r: 's' },
      'ad.resource': 'ag',
      'ad.title': 'ah',
      'ad.ignore': false,
      'ad.afterStop': false,
      'extraparam.1': 't',
      'extraparam.2': 'u',
      'extraparam.3': 'v',
      'extraparam.4': 'w',
      'extraparam.5': 'x',
      'extraparam.6': 'y',
      'extraparam.7': 'z',
      'extraparam.8': 'aa',
      'extraparam.9': 'ab',
      'extraparam.10': 'ac',
      'extraparam.11': 'ai',
      'extraparam.12': 'aj',
      'extraparam.13': 'ak',
      'extraparam.14': 'al',
      'extraparam.15': 'am',
      'extraparam.16': 'an',
      'extraparam.17': 'ao',
      'extraparam.18': 'ap',
      'extraparam.19': 'aq',
      'extraparam.20': 'ar',
      'ad.extraparam.1': 'as',
      'ad.extraparam.2': 'at',
      'ad.extraparam.3': 'au',
      'ad.extraparam.4': 'av',
      'ad.extraparam.5': 'aw',
      'ad.extraparam.6': 'ax',
      'ad.extraparam.7': 'ay',
      'ad.extraparam.8': 'az',
      'ad.extraparam.9': 'ba',
      'ad.extraparam.10': 'bb',
    })

    expect(options['enabled']).toBe(false)
    expect(options['httpSecure']).toBe(true)
    expect(options['host']).toBe('a')
    expect(options['accountCode']).toBe('b')
    expect(options['username']).toBe('c')
    expect(options['obfuscateIp']).toBe(false)
    expect(options['userType']).toBe('ad')
    expect(options['parse.hls']).toBe(true)
    expect(options['parse.cdnNameHeader']).toBe('d')
    expect(options['parse.cdnNode']).toBe(true)
    expect(options['parse.cdnNode.list'][0]).toBe('e')
    expect(options['parse.locationHeader']).toBe(false)
    expect(options['network.ip']).toBe('f')
    expect(options['network.isp']).toBe('g')
    expect(options['network.connectionType']).toBe('h')
    expect(options['device.code']).toBe('i')
    expect(options['content.resource']).toBe('j')
    expect(options['content.isLive']).toBe(true)
    expect(options['content.title']).toBe('k')
    expect(options['content.title2']).toBe('l')
    expect(options['content.duration']).toBe(1)
    expect(options['content.transactionCode']).toBe('m')
    expect(options['content.bitrate']).toBe(2)
    expect(options['content.throughput']).toBe(3)
    expect(options['content.rendition']).toBe('n')
    expect(options['content.cdn']).toBe('o')
    expect(options['content.metadata'].p).toBe('q')
    expect(options['content.cdnNode']).toBe('ae')
    expect(options['content.fps']).toBe('af')
    expect(options['content.cdnType']).toBe(1)
    expect(options['content.streamingProtocol']).toBe('HLS')
    expect(options['ad.metadata'].r).toBe('s')
    expect(options['ad.resource']).toBe('ag')
    expect(options['ad.title']).toBe('ah')
    expect(options['ad.ignore']).toBe(false)
    expect(options['ad.afterStop']).toBe(false)
    expect(options['extraparam.1']).toBe('t')
    expect(options['extraparam.2']).toBe('u')
    expect(options['extraparam.3']).toBe('v')
    expect(options['extraparam.4']).toBe('w')
    expect(options['extraparam.5']).toBe('x')
    expect(options['extraparam.6']).toBe('y')
    expect(options['extraparam.7']).toBe('z')
    expect(options['extraparam.8']).toBe('aa')
    expect(options['extraparam.9']).toBe('ab')
    expect(options['extraparam.10']).toBe('ac')
    expect(options['extraparam.11']).toBe('ai')
    expect(options['extraparam.12']).toBe('aj')
    expect(options['extraparam.13']).toBe('ak')
    expect(options['extraparam.14']).toBe('al')
    expect(options['extraparam.15']).toBe('am')
    expect(options['extraparam.16']).toBe('an')
    expect(options['extraparam.17']).toBe('ao')
    expect(options['extraparam.18']).toBe('ap')
    expect(options['extraparam.19']).toBe('aq')
    expect(options['extraparam.20']).toBe('ar')
    expect(options['ad.extraparam.1']).toBe('as')
    expect(options['ad.extraparam.2']).toBe('at')
    expect(options['ad.extraparam.3']).toBe('au')
    expect(options['ad.extraparam.4']).toBe('av')
    expect(options['ad.extraparam.5']).toBe('aw')
    expect(options['ad.extraparam.6']).toBe('ax')
    expect(options['ad.extraparam.7']).toBe('ay')
    expect(options['ad.extraparam.8']).toBe('az')
    expect(options['ad.extraparam.9']).toBe('ba')
    expect(options['ad.extraparam.10']).toBe('bb')
  })

  it('not to throw if a non-object is passed', () => {
    var options = new Options()
    expect(function () { options.setOptions(1) }).not.toThrow()
  })

  it('not to throw if a non-object is passed as base', () => {
    var options = new Options()
    expect(function () { options.setOptions({ 'parse.cdnNameHeader': 'd' }, 1) }).not.toThrow()
  })
})
