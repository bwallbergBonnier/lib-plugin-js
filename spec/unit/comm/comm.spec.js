describe('Communication', () => {
  var Communication = require('../../../src/comm/communication')
  var comm
  var t = {
    isBlocking: () => { return true },
    parse: () => {},
    on: () => {}
  }

  beforeEach(() => {
    global.spyXHR()
    comm = new Communication()
  })

  it('should add and remove transforms', () => {
    var t1 = t
    var t2 = t
    comm.addTransform(t1)
    comm.addTransform(t2)

    expect(comm.transforms.length).toBe(2)

    comm.removeTransform(t1)
    expect(comm.transforms.length).toBe(1)
  })

  it('should not fail when adding a wrong transform', () => {
    comm.addTransform({})
    expect(comm.transforms.length).toBe(0)
  })

  it('should not fail when trying to remove unexisting transforms', () => {
    comm.addTransform(t)
    comm.removeTransform({})
    expect(comm.transforms.length).toBe(1)
  })

  it('should add and block requests', () => {
    var trans = t
    comm.addTransform(trans)

    comm.buildRequest()
    expect(comm._requests.length).toBe(1)

    comm.sendRequest({})
    expect(comm._requests.length).toBe(2)
  })

  it('should register callbacks and requests', () => {
    var req = jasmine.createSpyObj('request', ['on', 'send'])
    var cb = () => {}
    comm.sendRequest(req, cb)

    expect(req.on).toHaveBeenCalledWith('load', cb)
    expect(req.send).toHaveBeenCalled()
  })
})
