describe('ViewTransform', () => {
  var ViewTransform = require('../../../../src/comm/transform/view')
  var vt

  beforeEach(() => {
    var plugin = {
      options: {},
      getHost: () => { return 'http://google.com' },
      adapter: {},
      requestBuilder: {
        buildParams: function () {
          return {
            system: 'system',
            pluginVersion: '6.0.0'
          }
        }
      }
    }
    vt = new ViewTransform(plugin)

    var req = {
      getXHR: () => {
        return {
          response: '{"q":{"h":"debug-nqs-lw2.nice264.com",' +
          '"t":"","pt":"5","c":"L_19057_dytt4ca2x2j7ymj","tc":"","b":"0"}}'
        }
      }
    }
    vt._receiveData(req)
  })

  it('should parse response', () => {
    expect(vt.response.host).toBe('http://debug-nqs-lw2.nice264.com')
    expect(vt.response.code).toBe('L_19057_dytt4ca2x2j7ymj')
    expect(vt.getViewCode()).toBe('L_19057_dytt4ca2x2j7ymj_-1')
    expect(vt.response.pingTime).toBe('5')
  })

  it('should transform pings', () => {
    var ping = { service: '/ping', params: {} }
    vt.nextView()
    vt.parse(ping)
    expect(ping.host).toBe('http://debug-nqs-lw2.nice264.com')
    expect(ping.params.code).toBe('L_19057_dytt4ca2x2j7ymj_0')
    expect(ping.params.pingTime).toBe('5')
  })
})
