describe('LocationHeaderTransform', () => {
  var LhParser = require('../../../../../src/comm/transform/resourceparsers/locationheaderparser')
  var ht

  beforeEach(() => {
    lhp = new LhParser()
  })

  it('should parse', () => {
    lhp._parseResponse('Location: http://www.example.com')
    expect(lhp.getResource()).toBe('http://www.example.com')
  })
})
