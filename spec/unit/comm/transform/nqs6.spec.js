describe('Nqs6Transform', () => {
  var Nqs6Transform = require('../../../../src/comm/transform/nqs6')
  var t = new Nqs6Transform()

  it('should copy common params', () => {
    var r = {
      params: {
        'accountCode': 'a',
        'transactionCode': 'b',
        'username': 'c',
        'mediaResource': 'd',
        'msg': 'e',
        'playhead': 1
      }
    }
    t.parse(r)

    expect(r.params.system).toBe('a')
    expect(r.params.transcode).toBe('b')
    expect(r.params.user).toBe('c')
    expect(r.params.resource).toBe('d')
    expect(r.params.msg).toBe('e')
    expect(r.params.time).toBe(1)
  })

  it('should copy start params', () => {
    var r = {
      service: '/start',
      params: {
        'mediaDuration': 1
      }
    }
    t.parse(r)

    expect(r.params.duration).toBe(1)
  })

  it('should copy join params', () => {
    var r = {
      service: '/joinTime',
      params: {
        'joinDuration': 1,
        'playhead': 2

      }
    }
    t.parse(r)

    expect(r.params.time).toBe(1)
    expect(r.params.eventTime).toBe(2)
  })

  it('should copy seek params', () => {
    var r = {
      service: '/seek',
      params: {
        'seekDuration': 1
      }
    }
    t.parse(r)

    expect(r.params.duration).toBe(1)
  })

  it('should copy buffer params', () => {
    var r = {
      service: '/bufferUnderrun',
      params: {
        'bufferDuration': 1
      }
    }
    t.parse(r)

    expect(r.params.duration).toBe(1)
  })

  it('should copy ping params', () => {
    var r = {
      service: '/ping',
      params: {
        'entities': { a: 1 }
      }
    }
    t.parse(r)

    expect(r.params.entityType).toBe('a')
    expect(r.params.entityValue).toBe(1)
  })
})
