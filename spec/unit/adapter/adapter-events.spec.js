describe('GenericAdapter Events', () => {
  var Adapter = require('../../../src/adapter/adapter')

  it('should /init', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.INIT, (e) => {
      expect(e.type).toBe(Adapter.Event.INIT)
      done()
    })
    adapter.fireInit()

    expect(adapter.flags.isInited).toBe(true)
    expect(adapter.chronos.join.startTime).not.toBe(0)
    expect(adapter.chronos.total.startTime).not.toBe(0)
  })

  it('should /start', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.START, (e) => {
      expect(e.type).toBe(Adapter.Event.START)
      done()
    })
    adapter.fireStart()

    expect(adapter.flags.isStarted).toBe(true)
    expect(adapter.chronos.join.startTime).not.toBe(0)
    expect(adapter.chronos.total.startTime).not.toBe(0)
  })

  it('should /join', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.JOIN, (e) => {
      expect(e.type).toBe(Adapter.Event.JOIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()

    expect(adapter.flags.isJoined).toBe(true)
    expect(adapter.chronos.join.stopTime).not.toBe(0)
  })

  it('should /pause', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.PAUSE, (e) => {
      expect(e.type).toBe(Adapter.Event.PAUSE)
      done()
    })

    adapter.fireStart()
    adapter.fireJoin()
    adapter.firePause()

    expect(adapter.flags.isPaused).toBe(true)
    expect(adapter.chronos.pause.startTime).not.toBe(0)
  })

  it('should /resume', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.RESUME, (e) => {
      expect(e.type).toBe(Adapter.Event.RESUME)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.firePause()
    adapter.fireResume()

    expect(adapter.flags.isPaused).toBe(false)
    expect(adapter.chronos.pause.stopTime).not.toBe(0)
  })

  it('should convert buffer from seek', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.fireBufferBegin({}, true)

    expect(adapter.flags.isBuffering).toBe(true)
    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.buffer.startTime).not.toBe(0)
    expect(adapter.chronos.seek.startTime).toBe(0)
  })

  it('should /buffer-begin', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()

    expect(adapter.flags.isBuffering).toBe(true)
    expect(adapter.chronos.buffer.startTime).not.toBe(0)
  })

  it('should /buffer-end', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BUFFER_END, (e) => {
      expect(e.type).toBe(Adapter.Event.BUFFER_END)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.fireBufferEnd()

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.chronos.buffer.stopTime).not.toBe(0)
  })

  it('should not /buffer-end, but cancel buffer', () => {
    var adapter = new Adapter(true)
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.cancelBuffer()

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.chronos.buffer.stopTime).not.toBe(0)
  })

  it('should convert seek from buffer', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.SEEK_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireBufferBegin()
    adapter.fireSeekBegin({}, true)

    expect(adapter.flags.isBuffering).toBe(false)
    expect(adapter.flags.isSeeking).toBe(true)
    expect(adapter.chronos.buffer.startTime).toBe(0)
    expect(adapter.chronos.seek.startTime).not.toBe(0)
  })

  it('should /seek-begin', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.SEEK_BEGIN, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_BEGIN)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()

    expect(adapter.flags.isSeeking).toBe(true)
    expect(adapter.chronos.seek.startTime).not.toBe(0)
  })

  it('should /seek-end', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.SEEK_END, (e) => {
      expect(e.type).toBe(Adapter.Event.SEEK_END)
      done()
    })
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.fireSeekEnd()

    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.seek.stopTime).not.toBe(0)
  })

  it('should not /seek-end, but cancel seek', () => {
    var adapter = new Adapter(true)
    adapter.fireStart()
    adapter.fireJoin()
    adapter.fireSeekBegin()
    adapter.cancelSeek()

    expect(adapter.flags.isSeeking).toBe(false)
    expect(adapter.chronos.seek.stopTime).not.toBe(0)
  })

  it('should /stop', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })

    adapter.fireStart()
    adapter.fireStop()

    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.chronos.total.fireStopTime).not.toBe(0)
  })

  it('should /error', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })

    adapter.fireError()
  })

  it('should /fatalError', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      expect(e.data.params.errorLevel).toBe('fatal')
      done()
    })

    adapter.fireFatalError()
  })

  it('should /stop (casted)', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.STOP, (e) => {
      expect(e.type).toBe(Adapter.Event.STOP)
      done()
    })

    adapter.fireStart()
    adapter.fireCasted()

    expect(adapter.flags.isStarted).toBe(false)
    expect(adapter.chronos.total.fireStopTime).not.toBe(0)
  })

  it('should /adClick', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.CLICK, (e) => {
      expect(e.type).toBe(Adapter.Event.CLICK)
      done()
    })

    adapter.fireStart()
    adapter.fireClick()
  })

  it('should /adBlocked', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.BLOCKED, (e) => {
      expect(e.type).toBe(Adapter.Event.BLOCKED)
      done()
    })

    adapter.fireStart()
    adapter.fireBlocked()
  })

  it('should /adError, no served ad', (done) => {
    var adapter = new Adapter(true)
    adapter.on(Adapter.Event.ERROR, (e) => {
      expect(e.type).toBe(Adapter.Event.ERROR)
      done()
    })

    adapter.fireStart()
    adapter.fireNoServedAd()
  })
})
