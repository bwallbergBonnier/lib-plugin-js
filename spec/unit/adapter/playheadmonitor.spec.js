describe('PlayheadMonitor', () => {
  var PlayheadMonitor = require('../../../src/adapter/playheadmonitor.js')

  describe('seek-detector', () => {
    var adapter

    beforeEach((done) => {
      adapter = {
        playhead: 0,
        getPlayhead: function () { return this.playhead },
        on: function (e) { },
        flags: {},
        fireSeekBegin: function (e) {
          pm.stop()
          clearInterval(int)
          done()
        },
        fireSeekEnd: function () { },
        fireBufferBegin: function () { },
        fireBufferEnd: function () { }
      }

      spyOn(adapter, 'fireSeekBegin').and.callThrough()

      var pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.SEEK, 200)
      pm.start()

      var int = setInterval(() => {
        adapter.playhead += 300
      }, 100)
    })

    it('should detect seeks', () => {
      expect(adapter.fireSeekBegin).toHaveBeenCalled()
    })
  })

  describe('buffer-detector', () => {
    var adapter

    beforeEach((done) => {
      adapter = {
        playhead: 1,
        getPlayhead: function () { return this.playhead },
        on: function (e) { },
        flags: {},
        fireSeekBegin: function () { },
        fireSeekEnd: function () { },
        fireBufferBegin: function (e) {
          pm.stop()
          done()
        },
        fireBufferEnd: function () { }
      }

      spyOn(adapter, 'fireBufferBegin').and.callThrough()

      var pm = new PlayheadMonitor(adapter, PlayheadMonitor.Type.BUFFER, 200)
      pm.start()
    })

    it('should detect buffers', () => {
      expect(adapter.fireBufferBegin).toHaveBeenCalled()
    })
  })
})
