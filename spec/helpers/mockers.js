var Mock = require('mock-browser')
var mock = new Mock.mocks.MockBrowser()

global.window = mock.getWindow()
global.document = mock.getDocument()
global.localStorage = global.sessionStorage = new Mock.mocks.MockStorage()
global.XMLHttpRequest = function () { }
global.spyXHR = function () {
  global.XMLHttpRequest.prototype = jasmine.createSpyObj('prototype', [
    'addEventListener',
    'removeEventListener',
    'open',
    'send'
  ])
}
