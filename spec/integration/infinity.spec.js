describe('Plugin Infinity Integration', () => {
  var Plugin = require('../../src/plugin/plugin')
  var YInfinity = require('../../src/infinity/infinity')
  var Log = require('../../src/log')
  var plugin

  beforeEach(() => {
    global.spyXHR()
    plugin = new Plugin({ 'accountCode': 'nicedev' })
    plugin._initInfinity()
  })

  it('should fire session start', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_SESSION_START, done)
    plugin.infinity.emit(YInfinity.Event.SESSION_START)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire session stop', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_SESSION_STOP, done)
    plugin.infinity.emit(YInfinity.Event.SESSION_STOP)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire session event', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_EVENT, done)
    plugin.infinity.emit(YInfinity.Event.EVENT)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire session nav', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_NAV, done)
    plugin.infinity.emit(YInfinity.Event.NAV)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire sendBeat', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_BEAT, done)
    plugin._sendBeat()
  })
})
