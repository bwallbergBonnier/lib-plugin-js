describe('Plugin Integration', () => {
  var Plugin = require('../../src/plugin/plugin')
  var Adapter = require('../../src/adapter/adapter')
  var Log = require('../../src/log')
  var plugin, adapter

  beforeEach(() => {
    global.spyXHR()
    plugin = new Plugin({ 'accountCode': 'nicedev' })
    adapter = new Adapter()
    plugin.setAdapter(adapter)
  })

  it('should fire start', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_START, done)
    adapter.emit(Adapter.Event.START)
  })

  it('should fire join', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_JOIN, done)
    adapter.emit(Adapter.Event.JOIN)
  })

  it('should fire pause', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_PAUSE, done)
    adapter.emit(Adapter.Event.PAUSE)
  })

  it('should fire resume', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_RESUME, done)
    adapter.emit(Adapter.Event.RESUME)
  })

  it('should listen seek-begin', () => {
    spyOn(Log, 'notice').and.callThrough()
    adapter.emit(Adapter.Event.SEEK_BEGIN)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire seek-end', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_SEEK, done)
    adapter.emit(Adapter.Event.SEEK_END)
  })

  it('should listen buffer-begin', () => {
    spyOn(Log, 'notice').and.callThrough()
    adapter.emit(Adapter.Event.BUFFER_BEGIN)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire buffer-end', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_BUFFER, done)
    adapter.emit(Adapter.Event.BUFFER_END)
  })

  it('should fire stop', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_STOP, done)
    adapter.emit(Adapter.Event.STOP)
  })

  it('should fire error', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_ERROR, done)
    adapter.emit(Adapter.Event.ERROR)
  })

  it('should fire sendPing', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_PING, done)
    plugin._sendPing()
  })
})
