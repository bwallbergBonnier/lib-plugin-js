describe('Ads Plugin Integration', () => {
  var Plugin = require('../../src/plugin/plugin')
  var Adapter = require('../../src/adapter/adapter')
  var Log = require('../../src/log')
  var plugin, adapter

  beforeEach(() => {
    global.spyXHR()
    plugin = new Plugin({ 'accountCode': 'nicedev' })
    adapter = new Adapter()
    plugin.setAdsAdapter(adapter)
  })

  it('should close buffers when starting', () => {
    var content = new Adapter()
    content.fireStart()
    content.fireJoin()
    content.fireBufferBegin({}, false)
    content.firePause()
    plugin.setAdapter(content)

    expect(content.flags.isBuffering).toBe(true)

    adapter.emit(Adapter.Event.START)

    expect(content.flags.isBuffering).toBe(false)
    expect(content.chronos.pause.startTime).toBe(0)
  })

  it('should close seeks when starting', () => {
    var content = new Adapter()
    content.fireStart()
    content.fireJoin()
    content.fireSeekBegin({}, false)
    content.firePause()
    plugin.setAdapter(content)

    expect(content.flags.isSeeking).toBe(true)

    adapter.emit(Adapter.Event.START)

    expect(content.flags.isSeeking).toBe(false)
    expect(content.chronos.pause.startTime).toBe(0)
  })

  it('should fire start', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_START, done)
    adapter.emit(Adapter.Event.INIT)
  })

  it('should fire join', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_JOIN, done)
    adapter.emit(Adapter.Event.JOIN)
  })

  it('should fire pause', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_PAUSE, done)
    adapter.emit(Adapter.Event.PAUSE)
  })

  it('should fire resume', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_RESUME, done)
    adapter.emit(Adapter.Event.RESUME)
  })

  it('should listen buffer-begin', () => {
    spyOn(Log, 'notice').and.callThrough()
    adapter.emit(Adapter.Event.BUFFER_BEGIN)
    expect(Log.notice).toHaveBeenCalled()
  })

  it('should fire buffer-end', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_BUFFER, done)
    adapter.emit(Adapter.Event.BUFFER_END)
  })

  it('should fire stop', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_STOP, done)
    adapter.emit(Adapter.Event.STOP)
  })

  it('should fire error', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_ERROR, done)
    adapter.emit(Adapter.Event.ERROR)
  })

  it('should fire click', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_CLICK, done)
    adapter.emit(Adapter.Event.CLICK)
  })

  it('should fire blocked', (done) => {
    plugin.on(Plugin.Event.WILL_SEND_AD_BLOCKED, done)
    adapter.emit(Adapter.Event.BLOCKED)
  })
})
