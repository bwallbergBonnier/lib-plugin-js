// This file is designed to add extra functionalities to Plugin class

var PluginInfinityGettersMixin = {
  getSessionId: function () {
    return this.storage.getLocal('session')
  },

  getContext: function () {
    return this.storage.getSession('context')
  },

  getStoredData: function () {
    return this.storage.getSession('data')
  },

  getPageName: function () {
    if (document && document.title) {
      return document.title
    }
  },

  getIsSessionExpired: function () {
    var time = this.storage.getSession('lastactive') + this.options['session.expire']
    var now = new Date().getTime()
    return !(this.getSessionId() && (time > now))
  },

  getIsDataExpired: function () {
    var time = this.storage.getSession('lastactive') + this.options['session.expire']
    var now = new Date().getTime()
    return !(this.getStoredData() && (time > now))
  }
}

module.exports = PluginInfinityGettersMixin
