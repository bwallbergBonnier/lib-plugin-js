var Log = require('../log')
var Constants = require('../constants')

var Adapter = require('../adapter/adapter')

// This file is designed to add extra functionalities to Plugin class

var PluginAdsMixin = {
  /**
   * Returns current adapter or null.
   *
   * @returns {Adapter}
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdsAdapter: function () {
    return this._adsAdapter
  },

  /**
   * Sets an adapter for ads.
   *
   * @param {Adapter} adsAdapter
   *
   * @memberof youbora.Plugin.prototype
   */
  setAdsAdapter: function (adsAdapter) {
    if (adsAdapter.plugin) {
      Log.warn('Adapters can only be added to a single plugin')
    } else {
      this.removeAdsAdapter()

      adsAdapter.plugin = this
      this._adsAdapter = adsAdapter
      this._adsAdapter.on(Adapter.Event.INIT, this._adInitListener.bind(this))
      this._adsAdapter.on(Adapter.Event.START, this._adStartListener.bind(this))
      this._adsAdapter.on(Adapter.Event.JOIN, this._adJoinListener.bind(this))
      this._adsAdapter.on(Adapter.Event.PAUSE, this._adPauseListener.bind(this))
      this._adsAdapter.on(Adapter.Event.RESUME, this._adResumeListener.bind(this))
      this._adsAdapter.on(Adapter.Event.BUFFER_BEGIN, this._adBufferBeginListener.bind(this))
      this._adsAdapter.on(Adapter.Event.BUFFER_END, this._adBufferEndListener.bind(this))
      this._adsAdapter.on(Adapter.Event.STOP, this._adStopListener.bind(this))
      this._adsAdapter.on(Adapter.Event.ERROR, this._adErrorListener.bind(this))
      this._adsAdapter.on(Adapter.Event.CLICK, this._adClickListener.bind(this))
      this._adsAdapter.on(Adapter.Event.BLOCKED, this._adBlockedListener.bind(this))
    }
  },

  /**
   * Removes the current adapter. Fires stop if needed. Calls adapter.dispose()
   *
   * @memberof youbora.Plugin.prototype
   */
  removeAdsAdapter: function () {
    if (this._adsAdapter) {
      this._adsAdapter.dispose()

      this._adsAdapter.plugin = null
      this._adsAdapter.off(Adapter.Event.INIT, this._adInitListener)
      this._adsAdapter.off(Adapter.Event.START, this._adStartListener)
      this._adsAdapter.off(Adapter.Event.JOIN, this._adJoinListener)
      this._adsAdapter.off(Adapter.Event.PAUSE, this._adPauseListener)
      this._adsAdapter.off(Adapter.Event.RESUME, this._adResumeListener)
      this._adsAdapter.off(Adapter.Event.BUFFER_BEGIN, this._adBufferBeginListener)
      this._adsAdapter.off(Adapter.Event.BUFFER_END, this._adBufferEndListener)
      this._adsAdapter.off(Adapter.Event.STOP, this._adStopListener)
      this._adsAdapter.off(Adapter.Event.ERROR, this._adErrorListener)
      this._adsAdapter.off(Adapter.Event.CLICK, this._adClickListener)
      this._adsAdapter.off(Adapter.Event.BLOCKED, this._adBlockedListener)

      this._adsAdapter = null
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------
  _adInitListener: function (e) {
    if (this._adapter) {
      this._adapter.fireBufferEnd()
      this._adapter.fireSeekEnd()
    }
    this.adInitSent = true
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.getNewAdNumber()
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_INIT, params)
    Log.notice(
      Constants.Service.AD_INIT + ' ' +
      params.adPosition + params.adNumber +
      ' at ' + params.playhead + 's'
    )
  },

  _adStartListener: function (e) {
    if (this._adapter) {
      this._adapter.fireBufferEnd()
      this._adapter.fireSeekEnd()
      if (this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    }

    var params = e.data.params || {}
    if (!this._adsAdapter.flags.isInited) {
      params.adNumber = this.requestBuilder.getNewAdNumber()
    } else {
      params.adNumber = this.requestBuilder.lastSent.adNumber
    }
    if (!this.options['ad.ignore']) {
      var allParamsReady = ((!!this._adsAdapter.getResource() || !!this._adsAdapter.getTitle())
        && typeof this._adsAdapter.getDuration() === 'number')
      if (allParamsReady) {
        this.adStartSent = true
        this._send(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_START, params)
        Log.notice(
          Constants.Service.AD_START + ' ' +
          params.adPosition + params.adNumber +
          ' at ' + params.playhead + 's'
        )
      }
      else {
        if (!this._adsAdapter.flags.isInited) {
          this.adInitSent = true
          this._send(Constants.WillSendEvent.WILL_SEND_AD_INIT, Constants.Service.AD_INIT, params)
          Log.notice(
            Constants.Service.AD_INIT + ' ' +
            params.adPosition + params.adNumber +
            ' at ' + params.playhead + 's'
          )
        }
      }
    }
  },

  _adJoinListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    if (!this.options['ad.ignore'] && this.adInitSent && !this.adStartSent) {
      this._send(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_START, params)
      Log.notice(
        Constants.Service.AD_START + ' ' +
        params.adPosition + params.adNumber +
        ' at ' + params.playhead + 's'
      )
    }
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_JOIN, Constants.Service.AD_JOIN, params)
    Log.notice(Constants.Service.AD_JOIN + ' ' + params.adJoinDuration + 'ms')
    this.adInitSent = false
    this.adStartSent = false
  },

  _adPauseListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_PAUSE, Constants.Service.AD_PAUSE, params)
    Log.notice(Constants.Service.AD_PAUSE + ' at ' + params.adPlayhead + 's')
  },

  _adResumeListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_RESUME, Constants.Service.AD_RESUME, params)
    Log.notice(Constants.Service.AD_RESUME + ' ' + params.adPauseDuration + 'ms')
  },

  _adBufferBeginListener: function (e) {
    Log.notice('Ad Buffer Begin')
    if (this._adsAdapter && this._adsAdapter.flags.isPaused) {
      this._adsAdapter.chronos.pause.reset()
    }
  },

  _adBufferEndListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_BUFFER, Constants.Service.AD_BUFFER, params)
    Log.notice(Constants.Service.AD_BUFFER + ' ' + params.adBufferDuration + 'ms')
  },

  _adStopListener: function (e) {
    // remove time from joinDuration
    if (this._adapter && !this._adapter.flags.isJoined) {
      this._adapter.chronos.join.startTime = Math.min(
        this._adapter.chronos.join.startTime + this._adsAdapter.chronos.total.getDeltaTime(),
        new Date().getTime()
      )
    }

    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_STOP, Constants.Service.AD_STOP, params)
    Log.notice(Constants.Service.AD_STOP + ' ' + params.adTotalDuration + 'ms')
  },

  _adErrorListener: function (e) {
    var params = e.data.params || {}
    if (this._adapter && !this._adapter.flags.isStarted) {
      params.adNumber = this.requestBuilder.lastSent.adNumber
    } else {
      params.adNumber = this.requestBuilder.getNewAdNumber()
    }
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_ERROR, Constants.Service.AD_ERROR, params)
    Log.notice(Constants.Service.AD_ERROR)
  },

  _adClickListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.lastSent.adNumber
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_CLICK, Constants.Service.AD_CLICK, params)
    Log.notice(Constants.Service.AD_CLICK)
  },

  _adBlockedListener: function (e) {
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.getNewAdNumber()
    if (!this.options['ad.ignore']) this._send(Constants.WillSendEvent.WILL_SEND_AD_BLOCKED, Constants.Service.AD_BLOCKED, params)
    Log.notice(Constants.Service.AD_BLOCKED)
  }
}

module.exports = PluginAdsMixin
