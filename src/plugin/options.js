var YouboraObject = require('../object')

var Options = YouboraObject.extend(
  /** @lends youbora.Options.prototype */
  {
    /**
     * This Class store youbora configuration settings.
     * Any value specified in this class, if set, will override the info the plugin is able to get
     * on its own.
     *
     * @constructs Options
     * @param {Object|Options} [options] A literal containing values.
     * @extends youbora.YouboraObject
     * @memberof youbora
     */
    constructor: function (options) {
      /** @prop {boolean} [enabled=true] If false, the plugin won't send NQS requests. */
      this['enabled'] = true

      /**
       * @prop {boolean} [httpSecure=null]
       * Define the security of NQS calls.
       * If true it will use 'https://',
       * if false it will use 'http://',
       * if null/undefined it will use '//'.
       */
      this['httpSecure'] = null

      /** @prop {string} [host='nqs.nice264.com'] Host of the Fastdata service. */
      this['host'] = 'nqs.nice264.com'

      /**
       * @prop {string} [accountCode='nicetest']
       * NicePeopleAtWork account code that indicates the customer account.
       */
      this['accountCode'] = 'nicetest'

      /** @prop {string} [username] User ID value inside your system. */
      this['username'] = null

      /**
       *  @prop {boolean} [obfuscateIp=false]
       * If true, the view will have the IP obfuscated
      */
      this['obfuscateIp'] = false

      /** @prop {string} [userType] User type. */
      this['userType'] = null

      // PARSERS

      /**
       * @prop {boolean} [parse.Hls=false]
       * If true the plugin will parse HLS files to use the first .ts file found as resource.
       * It might slow performance down.
       */
      this['parse.hls'] = false

      /**
       * @prop {string} [parse.CdnNameHeader]
       * If defined, resource parse will try to fetch the CDN code from the custom header defined
       * by this property. ie: 'x-cdn-forward'
       */
      this['parse.cdnNameHeader'] = 'x-cdn-forward'

      /**
       * @prop {boolean} [parse.CdnNode=false]
       * If true the plugin will query the CDN to retrieve the node name.
       * It might slow performance down.
       */
      this['parse.cdnNode'] = false

      /**
       * @prop {array<string>} [parse.CdnNode.list=false]
       * If true the plugin will query the CDN to retrieve the node name.
       * It might slow performance down.
       */
      this['parse.cdnNode.list'] = ['Akamai', 'Cloudfront', 'Level3', 'Fastly', 'Highwinds']

      /**
       * @prop {boolean} [parse.CdnNode=false]
       * If true the plugin will look for location value in manifest header to retrieve the actual resource
       * It might slow performance down.
       */
      this['parse.locationHeader'] = false

      // NETWORK

      /** @prop {string} [network.ip] IP of the viewer/user. ie= '100.100.100.100'. */
      this['network.ip'] = null

      /** @prop {string} [network.isp] Name of the internet service provider of the viewer/user. */
      this['network.isp'] = null

      /**
       * @prop {string} [network.connectionType]
       * See a list of codes in {@link http://mapi.youbora.com:8081/connectionTypes}
       */
      this['network.connectionType'] = null

      // DEVICE

      /**
       * @prop {string} [device.code]
       * Youbora's device code. If specified it will rewrite info gotten from user agent.
       * See a list of codes in {@link http://mapi.youbora.com:8081/devices}
       */
      this['device.code'] = null

      // CONTENT

      /** @prop {string} [content.transactionCode] Custom unique code to identify the view. */
      this['content.transactionCode'] = null

      /** @prop {string} [content.resource] URL/path of the current media resource. */
      this['content.resource'] = null

      /** @prop {boolean} [content.isLive] True if the content is live false if VOD. */
      this['content.isLive'] = null

      /** @prop {string} [content.title] Title of the media. */
      this['content.title'] = null

      /** @prop {string} [content.title2] Secondary title of the media. */
      this['content.title2'] = null

      /** @prop {number} [content.duration] Duration of the media. */
      this['content.duration'] = null

      /** @prop {int} [content.fps] Frames per second of the content in the current moment. */
      this['content.fps'] = null

      /** @prop {int} [content.bitrate] Bitrate of the content in bits per second. */
      this['content.bitrate'] = null

      /** @prop {int} [content.throughput] Throughput of the client bandwith in bits per second. */
      this['content.throughput'] = null

      /** @prop {string} [content.rendition] Name of the current rendition of the content. */
      this['content.rendition'] = null

      /**
       * @prop {string} [content.cdn]
       * Codename of the CDN where the content is streaming from.
       * See a list of codes in {@link http://mapi.youbora.com:8081/cdns}
       * */
      this['content.cdn'] = null

      /** @prop {string} [content.cdnNode] CDN node id */
      this['content.cdnNode'] = null

      /** @prop {int} [content.cdnType] CDN node type
       * TCP_HIT / HIT: 1
       * TCP_MISS / MISS: 2
       * TCP_MEM_HIT: 3
       * TCP_IMS_HIT: 4
      */
      this['content.cdnType'] = null

      /**
       * @prop {object} [content.metadata]
       * Item containing mixed extra information about the content like: director, parental rating,
       * device info or the audio channels.This object may store any serializable key:value info.
       */
      this['content.metadata'] = {}

      /** @prop {string} [content.streamingProtocol] Name of the streaming media protocol.
       * Can be:
       *   - HDS	(Adobe HDS)
       *   - HLS	(Apple HLS)
       *   - MSS	(Microsoft Smooth Streaming)
       *   - DASH	(MPEG-DASH)
       *   - RTMP	(Adobe RTMP)
       *   - RTP	(RTP)
       *   - RTSP	(RTSP)
       */
      this['content.streamingProtocol'] = null

      // ADS

      /**
       * @prop {object} [ad.metadata]
       * Item containing mixed extra information about ads like: request url.
       * This object may store any serializable key:value info.
       */
      this['ad.metadata'] = {}

      /**
        * @prop {string} [ad.campaign]
        * String containing the name of the campaign
        */
      this['ad.campaign'] = null

      /**
        * @prop {string} [ad.resource]
        * String containing the ad resource
        */
      this['ad.resource'] = null

      /**
        * @prop {string} [ad.title]
        * String containing the title of the campaign
        */
      this['ad.title'] = null

      /**
      * @prop {boolean} [ad.ignore]
      * False by default.
      * If true, youbora blocks ad events and calculates jointime ignoring ad time.
      */
      this['ad.ignore'] = false

      /**
      * @prop {int} [ad.afterStop]
      * 0 by default.
      * Set to integer positive value indicating how many ads
      * will be shown as post-rolls if they do it after content player triggers stop event.
      * Set to -1 to block automatic fireStop, manual call required.
      * Manual call using fireStop({end: true})
      */
      this['ad.afterStop'] = 0

      // EXTRAPARAMS

      /** @prop {string} [extraparam.1] Custom parameter 1. */
      this['extraparam.1'] = null

      /** @prop {string} [extraparam.2] Custom parameter 2. */
      this['extraparam.2'] = null

      /** @prop {string} [extraparam.3] Custom parameter 3. */
      this['extraparam.3'] = null

      /** @prop {string} [extraparam.4] Custom parameter 4. */
      this['extraparam.4'] = null

      /** @prop {string} [extraparam.5] Custom parameter 5. */
      this['extraparam.5'] = null

      /** @prop {string} [extraparam.6] Custom parameter 6. */
      this['extraparam.6'] = null

      /** @prop {string} [extraparam.7] Custom parameter 7. */
      this['extraparam.7'] = null

      /** @prop {string} [extraparam.8] Custom parameter 8. */
      this['extraparam.8'] = null

      /** @prop {string} [extraparam.9] Custom parameter 9. */
      this['extraparam.9'] = null

      /** @prop {string} [extraparam.10] Custom parameter 10. */
      this['extraparam.10'] = null

      /** @prop {string} [extraparam.11] Custom parameter 11. */
      this['extraparam.11'] = null

      /** @prop {string} [extraparam.12] Custom parameter 12. */
      this['extraparam.12'] = null

      /** @prop {string} [extraparam.13] Custom parameter 13. */
      this['extraparam.13'] = null

      /** @prop {string} [extraparam.14] Custom parameter 14. */
      this['extraparam.14'] = null

      /** @prop {string} [extraparam.15] Custom parameter 15. */
      this['extraparam.15'] = null

      /** @prop {string} [extraparam.16] Custom parameter 16. */
      this['extraparam.16'] = null

      /** @prop {string} [extraparam.17] Custom parameter 17. */
      this['extraparam.17'] = null

      /** @prop {string} [extraparam.18] Custom parameter 18. */
      this['extraparam.18'] = null

      /** @prop {string} [extraparam.19] Custom parameter 19. */
      this['extraparam.19'] = null

      /** @prop {string} [extraparam.20] Custom parameter 20. */
      this['extraparam.20'] = null

      /** @prop {string} [ad.extraparam.1] Ad custom parameter 1. */
      this['ad.extraparam.1'] = null

      /** @prop {string} [ad.extraparam.2] Ad custom parameter 2. */
      this['ad.extraparam.2'] = null

      /** @prop {string} [ad.extraparam.3] Ad custom parameter 3. */
      this['ad.extraparam.3'] = null

      /** @prop {string} [ad.extraparam.4] Ad custom parameter 4. */
      this['ad.extraparam.4'] = null

      /** @prop {string} [ad.extraparam.5] Ad custom parameter 5. */
      this['ad.extraparam.5'] = null

      /** @prop {string} [ad.extraparam.6] Ad custom parameter 6. */
      this['ad.extraparam.6'] = null

      /** @prop {string} [ad.extraparam.7] Ad custom parameter 7. */
      this['ad.extraparam.7'] = null

      /** @prop {string} [ad.extraparam.8] Ad custom parameter 8. */
      this['ad.extraparam.8'] = null

      /** @prop {string} [ad.extraparam.9] Ad custom parameter 9. */
      this['ad.extraparam.9'] = null

      /** @prop {string} [ad.extraparam.10] Ad custom parameter 10. */
      this['ad.extraparam.10'] = null

      /** @prop {number} [session.expire=21600000] Time until infinity session expires in ms. */
      this['session.expire'] = 21600000

      this.setOptions(options)
    },

    /**
     * Recursively sets the properties present in the params object.
     * ie: this.username = params.username.
     *
     * @param {Object} options A literal or another Data containing values.
     * @param {Object} [base=this] Start point for recursion.
     */
    setOptions: function (options, base) {
      base = base || this
      if (typeof options !== 'undefined') {
        for (var key in options) {
          if (typeof base[key] === 'object' && base[key] !== null) {
            this.setOptions(options[key], base[key])
          } else {
            base[key] = options[key]
          }
        }
      }
    }
  }
)

module.exports = Options
