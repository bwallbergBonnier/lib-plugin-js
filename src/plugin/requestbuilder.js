var YouboraObject = require('../object')
var Log = require('../log')

var startParams = [
  'accountCode',
  'username',
  'rendition',
  'player',
  'title',
  'title2',
  'live',
  'mediaDuration',
  'mediaResource',
  'transactionCode',
  'properties',
  'cdn',
  'playerVersion',
  'param1',
  'param2',
  'param3',
  'param4',
  'param5',
  'param6',
  'param7',
  'param8',
  'param9',
  'param10',
  'param11',
  'param12',
  'param13',
  'param14',
  'param15',
  'param16',
  'param17',
  'param18',
  'param19',
  'param20',
  'obfuscateIp',
  'p2pEnabled',
  'pluginVersion',
  'pluginInfo',
  'isp',
  'connectionType',
  'ip',
  'deviceCode',
  'preloadDuration',
  'referer',
  'userType',
  'streamingProtocol',
  'householdId'
]

var adStartParams = [
  'playhead',
  'adTitle',
  'adPosition',
  'adDuration',
  'adCampaign',
  'adResource',
  'adPlayerVersion',
  'adProperties',
  'adAdapterVersion',
  'extraparam1',
  'extraparam2',
  'extraparam3',
  'extraparam4',
  'extraparam5',
  'extraparam6',
  'extraparam7',
  'extraparam8',
  'extraparam9',
  'extraparam10'
]

var RequestBuilder = YouboraObject.extend(
  /** @lends youbora.RequestBuilder.prototype */
  {
    /**
     * This class helps building params associated with each event: /start, /joinTime...
     *
     * @constructs RequestBuilder
     * @extends youbora.YouboraObject
     * @memberof youbora
     *
     * @param {Plugin} plugin A Plugin instance
     */
    constructor: function (plugin) {
      this._plugin = plugin
      this._adNumber = 0

      /** Stores a list of the last params fetched */
      this.lastSent = {}
    },

    /**
     * Adds to params all the entities specified in paramList, unless they are already set.
     *
     * @param {Object} params Object of params key:value.
     * @param {Array.string} paramList A list of params to fetch.
     * @param {bool} onlyDifferent If true, only fetches params that have changed
     */
    fetchParams: function (params, paramList, onlyDifferent) {
      params = params || {}
      paramList = paramList || []
      for (var i = 0; i < paramList.length; i++) {
        var param = paramList[i]

        if (params[param]) { continue }
        var getterName = RequestBuilder.getters[param]

        if (this._plugin[getterName]) {
          var value = this._plugin[getterName]()
          if (value !== null && (!onlyDifferent || this.lastSent[param] !== value)) {
            params[param] = value
            this.lastSent[param] = value
          }
        } else {
          Log.warn('Trying to call undefined getter ' + param + ':' + getterName)
        }
      }

      return params
    },

    /**
     * Adds to params object all the entities specified in paramList, unless they are already set.
     *
     * @param {Object} params Object of params key:value.
     * @param {string} service The name of the service. Use {@link Plugin.Service} enum.
     */
    buildParams: function (params, service) {
      params = params || {}
      this.fetchParams(params, RequestBuilder.params[service], false)
      this.fetchParams(params, RequestBuilder.differentParams[service], true)
      return params
    },

    /**
     * Creates an adnumber if it does not exist and stores it in lastSent. If it already exists,
     * it is incremented by 1.
     *
     * @returns {number} adNumber
     */
    getNewAdNumber: function () {
      var adNumber = this.lastSent.adNumber
      if (adNumber && this.lastSent.adPosition === this._plugin.getAdPosition()) {
        adNumber += 1
      } else {
        adNumber = 1
      }
      this.lastSent.adNumber = adNumber
      return adNumber
    },

    /**
     * Return changed entities since last check
     *
     * @returns {Object} params
     */
    getChangedEntities: function () {
      return this.fetchParams({}, RequestBuilder.differentParams['entities'], true)
    }
  },
  /** @lends youbora.RequestBuilder */
  {
    // Static Members

    /** List of params used by each service */
    params: {
      '/data': ['system', 'pluginVersion', 'requestNumber'],

      '/init': startParams,
      '/start': startParams,
      '/joinTime': ['joinDuration', 'playhead', 'mediaDuration'],
      '/pause': ['playhead'],
      '/resume': ['pauseDuration', 'playhead'],
      '/seek': ['seekDuration', 'playhead'],
      '/bufferUnderrun': ['bufferDuration', 'playhead'],
      '/error': ['player'].concat(startParams),
      '/stop': ['bitrate', 'playhead', 'pauseDuration'],

      '/adInit': adStartParams,
      '/adStart': adStartParams,
      '/adJoin': ['playhead', 'adPosition', 'adJoinDuration', 'adPlayhead'],
      '/adPause': ['playhead', 'adPosition', 'adPlayhead'],
      '/adResume': ['playhead', 'adPosition', 'adPlayhead', 'adPauseDuration'],
      '/adBufferUnderrun': ['playhead', 'adPosition', 'adPlayhead', 'adBufferDuration'],
      '/adStop': ['playhead', 'adPosition', 'adPlayhead', 'adBitrate', 'adTotalDuration', 'pauseDuration'],
      '/adClick': ['playhead', 'adPosition', 'adPlayhead'],
      '/adError': adStartParams,
      '/adBlocked': adStartParams,
      '/ping': ['droppedFrames', 'playrate', 'cdnDownloadedTraffic', 'p2pDownloadedTraffic', 'uploadTraffic'],

      '/infinity/session/start': [
        'accountCode',
        'username',
        'navContext',
        'route',
        'page'
      ],
      '/infinity/session/stop': [],
      '/infinity/session/nav': ['navContext', 'username', 'route', 'page'],
      '/infinity/session/beat': [],
      '/infinity/event': ['accountCode']
    },

    /** List of params used by each service (only if they are different) */
    differentParams: {
      '/join': [
        'title',
        'title2',
        'live',
        'mediaDuration',
        'mediaResource'],
      '/adJoin': ['adTitle', 'adDuration', 'adResource'],
      'entities': [
        'rendition',
        'title',
        'title2',
        'live',
        'param1',
        'param2',
        'param3',
        'param4',
        'param5',
        'param6',
        'param7',
        'param8',
        'param9',
        'param10',
        'param11',
        'param12',
        'param13',
        'param14',
        'param15',
        'param16',
        'param17',
        'param18',
        'param19',
        'param20',
        'connectionType',
        'deviceCode',
        'ip',
        'username',
        'cdn',
        'nodeHost',
        'nodeType',
        'nodeTypeString'
      ]
    },

    /** List of params and its related getter */
    getters: {
      requestNumber: 'getRequestNumber',
      playhead: 'getPlayhead',
      playrate: 'getPlayrate',
      fps: 'getFramesPerSecond',
      droppedFrames: 'getDroppedFrames',
      mediaDuration: 'getDuration',
      bitrate: 'getBitrate',
      throughput: 'getThroughput',
      rendition: 'getRendition',
      title: 'getTitle',
      title2: 'getTitle2',
      live: 'getIsLive',
      mediaResource: 'getResource',
      transactionCode: 'getTransactionCode',
      properties: 'getMetadata',
      playerVersion: 'getPlayerVersion',
      player: 'getPlayerName',
      cdn: 'getCdn',
      pluginVersion: 'getPluginVersion',
      userType: 'getUserType',
      streamingProtocol: 'getStreamingProtocol',
      obfuscateIp: 'getObfuscateIp',
      householdId: 'getHouseholdId',

      param1: 'getExtraparam1',
      param2: 'getExtraparam2',
      param3: 'getExtraparam3',
      param4: 'getExtraparam4',
      param5: 'getExtraparam5',
      param6: 'getExtraparam6',
      param7: 'getExtraparam7',
      param8: 'getExtraparam8',
      param9: 'getExtraparam9',
      param10: 'getExtraparam10',
      param11: 'getExtraparam11',
      param12: 'getExtraparam12',
      param13: 'getExtraparam13',
      param14: 'getExtraparam14',
      param15: 'getExtraparam15',
      param16: 'getExtraparam16',
      param17: 'getExtraparam17',
      param18: 'getExtraparam18',
      param19: 'getExtraparam19',
      param20: 'getExtraparam20',

      extraparam1: 'getAdExtraparam1',
      extraparam2: 'getAdExtraparam2',
      extraparam3: 'getAdExtraparam3',
      extraparam4: 'getAdExtraparam4',
      extraparam5: 'getAdExtraparam5',
      extraparam6: 'getAdExtraparam6',
      extraparam7: 'getAdExtraparam7',
      extraparam8: 'getAdExtraparam8',
      extraparam9: 'getAdExtraparam9',
      extraparam10: 'getAdExtraparam10',

      adPosition: 'getAdPosition',
      adPlayhead: 'getAdPlayhead',
      adDuration: 'getAdDuration',
      adCampaign: 'getAdCampaign',
      adBitrate: 'getAdBitrate',
      adTitle: 'getAdTitle',
      adResource: 'getAdResource',
      adPlayerVersion: 'getAdPlayerVersion',
      adProperties: 'getAdMetadata',
      adAdapterVersion: 'getAdAdapterVersion',

      pluginInfo: 'getPluginInfo',

      isp: 'getIsp',
      connectionType: 'getConnectionType',
      ip: 'getIp',

      deviceCode: 'getDeviceCode',

      system: 'getAccountCode',
      accountCode: 'getAccountCode',
      username: 'getUsername',

      preloadDuration: 'getPreloadDuration',

      joinDuration: 'getJoinDuration',
      bufferDuration: 'getBufferDuration',
      seekDuration: 'getSeekDuration',
      pauseDuration: 'getPauseDuration',

      adJoinDuration: 'getAdJoinDuration',
      adBufferDuration: 'getAdBufferDuration',
      adPauseDuration: 'getAdPauseDuration',
      adTotalDuration: 'getAdTotalDuration',

      referer: 'getReferer',

      nodeHost: 'getNodeHost',
      nodeType: 'getNodeType',
      nodeTypeString: 'getNodeTypeString',

      route: 'getReferer',
      sessionId: 'getSessionId',
      navContext: 'getContext',
      page: 'getPageName',

      cdnDownloadedTraffic: 'getCdnTraffic',
      p2pDownloadedTraffic: 'getP2PTraffic',
      p2pEnabled: 'getIsP2PEnabled',
      uploadTraffic: 'getUploadTraffic'
    }

  }
)

module.exports = RequestBuilder
