var YouboraObject = require('../object')
var Log = require('../log')

var prefix = 'youbora'

/**
 * This class manages data sotrage in the browser memory.
 *
 * @extends youbora.Emitter
 * @memberof youbora
 */
var YouboraStorage = YouboraObject.extend(
  /** @lends youbora.YouboraStorage.prototype */
  {
    /**
     * Saves in localStorage or equivalent
     *
     * @param {string} key Key of the value. 'youbora.' will be appended.
     * @param {string} value Value.
     */
    setLocal: function (key, value) {
      try {
        if (!localStorage) {
          Log.error('Youbora Infinity needs localStorage which is not supported by your browser.')
          // TO-DO: develop alternative based on cookies
        } else {
          return localStorage.setItem('youbora.' + key, value)
        }
      }
      catch (err) {
        Log.error('Youbora Infinity needs localStorage which is not supported by your browser.')
      }
      return null
    },

    /**
     * Reads from localStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     */
    getLocal: function (key) {
      try {
        if (!localStorage) {
          Log.error('Youbora Infinity needs localStorage which is not supported by your browser.')
          // TO-DO: develop alternative based on cookies
        } else {
          return localStorage.getItem(prefix + '.' + key)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs localStorage which is not supported by your browser.')
      }
      return null
    },

    /**
     * Saves in sessionStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     * @param {string} value Value.
     */
    setSession: function (key, value) {
      try {
        if (!sessionStorage) {
          Log.error('Youbora Infinity needs sessionStorage which is not supported by your browser.')
          // TO-DO: develop alternative based on cookies
        } else {
          return sessionStorage.setItem(prefix + '.' + key, value)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs sessionStorage which is not supported by your browser.')
      }
      return null
    },

    /**
     * Reads from sessionStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     */
    getSession: function (key) {

      try {
        if (!sessionStorage) {
          Log.error('Youbora Infinity needs sessionStorage which is not supported by your browser.')
          // TO-DO: develop alternative based on cookies
        } else {
          return sessionStorage.getItem(prefix + '.' + key)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs sessionStorage which is not supported by your browser.')
      }
      return null
    }
  }
)

module.exports = YouboraStorage
