var Log = require('../log')
var Util = require('../util')
var Constants = require('../constants')

// This file is designed to add extra functionalities to Plugin class

/** @lends youbora.Plugin.prototype */
var PluginFireMixin = {
  /**
   * Starts preloading state and chronos.
   *
   * @memberof youbora.Plugin.prototype
   */
  firePreloadBegin: function () {
    if (!this.isPreloading) {
      this.isPreloading = true
      this.preloadChrono.start()
    }
  },

  /**
   * Ends preloading state and chronos.
   *
   * @memberof youbora.Plugin.prototype
   */
  firePreloadEnd: function () {
    if (this.isPreloading) {
      this.isPreloading = false
      this.preloadChrono.stop()
    }
  },

  /**
   * Sends /init. Should be called once the user has requested the content. Does not need
   * a working adapter or player to work. it won't sent start if isInitiated is true.
   *
   * @param {Object} [params] Object of key:value params.
   *
   * @memberof youbora.Plugin.prototype
   */
  fireInit: function (params) {
    if (!this.isInitiated) {
      if (!this.getAdapter() || (this.getAdapter() && !this.getAdapter().flags.isInited && !this.getAdapter().flags.isStarted)) {
        this.viewTransform.nextView()
        this._initComm()
        this._startPings()
        this.initChrono.start()
        this.isInitiated = true

        params = params || {}
        this._send(Constants.WillSendEvent.WILL_SEND_INIT, Constants.Service.INIT, params)
        Log.notice(Constants.Service.INIT + ' ' + (params.title || params.mediaResource))
      }
    }
  },

  /**
   * Sends /error. Should be used when the error is related to out-of-player errors: like async
   * resource load or player loading errors.
   *
   * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
   * @param {String} [msg] Error Message
   * @param {Object} [metadata] Object defining error metadata
   * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
   *
   * @memberof youbora.Plugin.prototype
   */
  fireError: function (object, msg, metadata, level) {
    if (!this.isInitiated && (!this.getAdapter() || (!this.getAdapter().flags.isStarted && !this.getAdapter().flags.isInited))) this.viewTransform.nextView()
    if (!this._comm) this._initComm()
    var params = Util.buildErrorParams(object, msg, metadata, level)

    this._send(Constants.WillSendEvent.WILL_SEND_ERROR, Constants.Service.ERROR, params)
    Log.notice(Constants.Service.ERROR +
      ' ' + params.errorLevel +
      ' ' + params.errorCode
    )

    if (params.errorLevel === 'fatal') {
      this._reset()
      this.viewTransform.nextView()
    }
  },

  /**
   * Calls fireErrors and then stops pings.
   *
   * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
   * @param {String} [msg] Error Message
   * @param {Object} [metadata] Object defining error metadata
   *
   * @memberof youbora.Plugin.prototype
   */
  fireFatalError: function (object, msg, metadata) {
    this.options['ad.afterStop'] = 0
    this.fireError(object, msg, metadata, 'fatal')
    if (this._adapter) { this._adapter.fireStop() } else { this.fireStop() }
  },

  /**
   * Fires /stop. Should be used to terminate sessions once the player is gone or if
   * plugin.fireError() is called.
   *
   * @param {Object} [params] Object of key:value params.
   *
   * @memberof youbora.Plugin.prototype
   */
  fireStop: function (params) {
    if (params && params.end !== undefined && params.end) {
      params.end = undefined
      this.options['ad.afterStop'] = 0
    }
    if (this._adapter) {
      this._adapter.flags.isEnded = true
      if (this._adapter.flags.isPaused) {
        if (!params) {
          params = {}
        }
        params.pauseDuration = this._adapter.chronos.pause.getDeltaTime()
      }
    }
    if (!this._adsAdapter) {
      this.options['ad.afterStop'] = 0
    }
    if (this.options['ad.afterStop'] === 0) {
      if (this._adapter) {
        this._adapter.flags.isStopped = true
      }
      params = params || {}
      this._send(Constants.WillSendEvent.WILL_SEND_STOP, Constants.Service.STOP, params)
      Log.notice(Constants.Service.STOP + ' at ' + params.playhead + 's')
      this._reset()
    }
  }
}

module.exports = PluginFireMixin
