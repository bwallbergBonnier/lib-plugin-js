var Log = require('../log')
var Util = require('../util')

var version = require('../version')

// This file is designed to add extra functionalities to Plugin class

var PluginContentGetterMixin = {
  /**
   * Returns content's playhead
   *
   * @memberof youbora.Plugin.prototype
   */
  getPlayhead: function () {
    var ret = 0
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayhead()
      } catch (err) {
        Log.warn('An error occurred while calling getPlayhead', err)
      }
    }
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns content's PlayRate
   *
   * @memberof youbora.Plugin.prototype
   */
  getPlayrate: function () {
    var ret = 0
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayrate()
      } catch (err) {
        Log.warn('An error occured while calling getPlayrate', err)
      }
    }
    return ret
  },

  /**
   * Returns content's FramesPerSecond
   *
   * @memberof youbora.Plugin.prototype
   */
  getFramesPerSecond: function () {
    var ret = this.options['content.fps']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getFramesPerSecond()
      } catch (err) {
        Log.warn('An error occured while calling getFramesPerSecond', err)
      }
    }
    return ret
  },

  /**
   * Returns content's DroppedFrames
   *
   * @memberof youbora.Plugin.prototype
   */
  getDroppedFrames: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getDroppedFrames()
      } catch (err) {
        Log.warn('An error occured while calling getDroppedFrames', err)
      }
    }
    if (!ret) {
      ret = this.getWebkitDroppedFrames()
    }
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns dropped frames as per webkitDroppedFrameCount
   *
   * @returns {number}
   *
   * @memberof youbora.Plugin.prototype
   */
  getWebkitDroppedFrames: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitDroppedFrameCount) {
      return this._adapter.tag.webkitDroppedFrameCount
    }
    return null
  },

  /**
   * Returns content's Duration
   *
   * @memberof youbora.Plugin.prototype
   */
  getDuration: function () {
    var ret = this.options['content.duration']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getDuration()
      } catch (err) {
        Log.warn('An error occured while calling getDuration', err)
      }
    }
    if (ret === 0) ret = null
    return Util.parseNumber(Math.round(ret), null)
  },

  /**
   * Returns content's Bitrate
   *
   * @memberof youbora.Plugin.prototype
   */
  getBitrate: function () {
    var ret = this.options['content.bitrate']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getBitrate()
      } catch (err) {
        Log.warn('An error occured while calling getBitrate', err)
      }

      if (!ret || ret === -1) {
        ret = this.getWebkitBitrate()
      }
    }
    return Util.parseNumber(ret, -1)
  },

  /**
   * Returns bitrate as per webkitVideoDecodedByteCount
   *
   * @param {Object} tag Video tag DOM reference.
   * @returns {number}
   *
   * @memberof youbora.Plugin.prototype
   */
  getWebkitBitrate: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitVideoDecodedByteCount) {
      var bitrate = this._adapter.tag.webkitVideoDecodedByteCount
      if (this._lastWebkitBitrate) {
        var delta = this._adapter.tag.webkitVideoDecodedByteCount - this._lastWebkitBitrate
        bitrate = Math.round(((delta) / this.viewTransform.response.pingTime) * 8)
      }
      this._lastWebkitBitrate = this._adapter.tag.webkitVideoDecodedByteCount
      return bitrate !== 0 ? bitrate : -1
    }
  },

  /**
   * Returns content's Throughput
   *
   * @memberof youbora.Plugin.prototype
   */
  getThroughput: function () {
    var ret = this.options['content.throughput']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getThroughput()
      } catch (err) {
        Log.warn('An error occured while calling getThroughput', err)
      }
    }
    return Util.parseNumber(ret, -1)
  },

  /**
   * Returns content's Rendition
   *
   * @memberof youbora.Plugin.prototype
   */
  getRendition: function () {
    var ret = this.options['content.rendition']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getRendition()
      } catch (err) {
        Log.warn('An error occured while calling getRendition', err)
      }
    }
    return ret
  },

  /**
   * Returns content's Title
   *
   * @memberof youbora.Plugin.prototype
   */
  getTitle: function () {
    var ret = this.options['content.title']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getTitle()
      } catch (err) {
        Log.warn('An error occured while calling getTitle', err)
      }
    }
    return ret
  },

  /**
   * Returns content's Title2
   *
   * @memberof youbora.Plugin.prototype
   */
  getTitle2: function () {
    var ret = this.options['content.title2']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getTitle2()
      } catch (err) {
        Log.warn('An error occured while calling getTitle2', err)
      }
    }
    return ret
  },

  /**
   * Returns content's IsLive
   *
   * @memberof youbora.Plugin.prototype
   */
  getIsLive: function () {
    var ret = this.options['content.isLive']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getIsLive()
      } catch (err) {
        Log.warn('An error occured while calling getIsLive', err)
      }
    }
    return ret || false
  },

  /**
   * Returns content's Resource after being parsed by the resourceTransform
   *
   * @memberof youbora.Plugin.prototype
   */
  getResource: function () {
    var ret = null
    if (!this.resourceTransform.isBlocking() && this.resourceTransform.isDone) {
      ret = this.resourceTransform.getResource()
    }

    return ret || this.getOriginalResource()
  },

  /**
   * Returns content's original Resource
   *
   * @memberof youbora.Plugin.prototype
   */
  getOriginalResource: function () {
    var ret = null
    ret = this.options['content.resource']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getResource()
      } catch (err) {
        Log.warn('An error occured while calling getResource', err)
        ret = null
      }
    }
    return ret || null
  },

  /**
   * Returns content's TransactionCode
   *
   * @memberof youbora.Plugin.prototype
   */
  getTransactionCode: function () {
    return this.options['content.transactionCode']
  },

  /**
   * Returns content's Metadata
   *
   * @memberof youbora.Plugin.prototype
   */
  getMetadata: function () {
    return this.options['content.metadata']
  },

  /**
   * Returns content's PlayerVersion
   *
   * @memberof youbora.Plugin.prototype
   */
  getPlayerVersion: function () {
    var ret = ''
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayerVersion()
      } catch (err) {
        Log.warn('An error occured while calling getPlayerVersion', err)
      }
    }
    return ret
  },

  /**
   * Returns content's PlayerName
   *
   * @memberof youbora.Plugin.prototype
   */
  getPlayerName: function () {
    var ret = ''
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayerName()
      } catch (err) {
        Log.warn('An error occured while calling getPlayerName', err)
      }
    }
    return ret
  },

  /**
   * Returns content's Cdn
   *
   * @memberof youbora.Plugin.prototype
   */
  getCdn: function () {
    var ret = null
    if (!this.resourceTransform.isBlocking()) {
      ret = this.resourceTransform.getCdnName()
    }
    return ret || this.options['content.cdn']
  },

  /**
   * Returns content's PluginVersion
   *
   * @memberof youbora.Plugin.prototype
   */
  getPluginVersion: function () {
    var ret = this.getAdapterVersion()
    if (!ret) ret = version + '-adapterless'

    return ret
  },

  /**
   * Returns ads adapter getVersion or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdapterVersion: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getVersion()
      } catch (err) {
        Log.warn('An error occured while calling getPluginVersion', err)
      }
    }
    return ret
  },

  /**
   * Returns cdn traffic received in bytes or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getCdnTraffic: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getCdnTraffic()
      } catch (err) {
        Log.warn('An error occured while calling getCdnTraffic', err)
      }
    }
    return ret
  },

  /**
   * Returns p2p traffic received in bytes or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getP2PTraffic: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getP2PTraffic()
      } catch (err) {
        Log.warn('An error occured while calling getP2PTraffic', err)
      }
    }
    return ret
  },


  /**
   * Returns p2p traffic sent in bytes or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getUploadTraffic: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getUploadTraffic()
      } catch (err) {
        Log.warn('An error occured while calling getUploadTraffic', err)
      }
    }
    return ret
  },

  /**
 * Returns if p2p plugin is enabled or null
 *
 * @memberof youbora.Plugin.prototype
 */
  getIsP2PEnabled: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getIsP2PEnabled()
      } catch (err) {
        Log.warn('An error occured while calling getIsP2PEnabled', err)
      }
    }
    return ret
  },


  getStreamingProtocol: function () {
    var ret = this.options['content.streamingProtocol']
    if (typeof ret != 'string') return null
    ret = ret.toUpperCase()
    if (ret != 'HDS' && ret != 'HLS' && ret != 'MSS' && ret != 'DASH' && ret != 'RTMP' && ret != 'RTP' && ret != 'RTSP') {
      Log.warn('Streaming protocol ' + ret + ' is not a valid value')
      return null
    }
    return ret
  },

  /**Returns household id*/
  getHouseholdId: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getHouseholdId()
      } catch (err) {
        Log.warn('An error occurred while calling getHouseholdId', err)
      }
    }
    return ret
  },
  // ----------------------------------------- CHRONOS ------------------------------------------

  /**
   * Returns preload chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getPreloadDuration: function () {
    return this.preloadChrono.getDeltaTime(false)
  },

  /**
   * Returns JoinDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getJoinDuration: function () {
    return this._adapter ? this._adapter.chronos.join.getDeltaTime(false) : -1
  },

  /**
   * Returns BufferDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getBufferDuration: function () {
    return this._adapter ? this._adapter.chronos.buffer.getDeltaTime(false) : -1
  },

  /**
   * Returns SeekDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getSeekDuration: function () {
    return this._adapter ? this._adapter.chronos.seek.getDeltaTime(false) : -1
  },

  /**
   * Returns pauseDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getPauseDuration: function () {
    return this._adapter ? this._adapter.chronos.pause.getDeltaTime(false) : 0
  }
}

module.exports = PluginContentGetterMixin
