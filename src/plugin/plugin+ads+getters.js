var Log = require('../log')
var Util = require('../util')

// This file is designed to add extra functionalities to Plugin class

var PluginAdsGettersMixin = {
  /**
   * Returns ads's PlayerVersion
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPlayerVersion: function () {
    var ret = ''
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getPlayerVersion()
      } catch (err) {
        Log.warn('An error occured while calling getAdPlayerVersion', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's position
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPosition: function () {
    var ret = 'pre'
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getPosition()
      } catch (err) {
        Log.warn('An error occured while calling getAdPosition', err)
      }
    }
    if (!ret && this._adapter) {
      ret = (this._adapter.flags.isJoined) ? 'mid' : 'pre'
    }
    return ret
  },

  /**
   * Returns ad's AdPlayhead
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPlayhead: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getPlayhead()
      } catch (err) {
        Log.warn('An error occured while calling getAdPlayhead', err)
      }
    }
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns ad's AdDuration
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdDuration: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getDuration()
      } catch (err) {
        Log.warn('An error occured while calling getAdDuration', err)
      }
    }
    return Util.parseNumber(ret, 0)
  },

  /**
   * Returns ad's AdBitrate
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdBitrate: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getBitrate()
      } catch (err) {
        Log.warn('An error occured while calling getAdBitrate', err)
      }

      if (!ret || ret === -1) {
        ret = this.getWebkitAdBitrate()
      }
    }
    return Util.parseNumber(ret, -1)
  },

  /**
   * Returns bitrate as per webkitVideoDecodedByteCount
   *
   * @param {Object} tag Video tag DOM reference.
   * @returns {number}
   *
   * @memberof youbora.Plugin.prototype
   */
  getWebkitAdBitrate: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitVideoDecodedByteCount) {
      var bitrate = this._adapter.tag.webkitVideoDecodedByteCount
      if (this._lastWebkitAdBitrate) {
        var delta = this._adapter.tag.webkitVideoDecodedByteCount - this._lastWebkitAdBitrate
        bitrate = Math.round(((delta) / this.viewTransform.response.pingTime) * 8)
      }
      this._lastWebkitAdBitrate = this._adapter.tag.webkitVideoDecodedByteCount
      return bitrate !== 0 ? bitrate : -1
    }
  },

  /**
   * Returns ad's AdTitle
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdTitle: function () {
    var ret = null
    if (this.options['ad.title']) {
      return this.options['ad.title']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getTitle()
      } catch (err) {
        Log.warn('An error occured while calling getAdTitle', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's AdResource
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdResource: function () {
    var ret = null
    if (this.options['ad.resource']) {
      return this.options['ad.resource']
    }
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getResource()
      } catch (err) {
        Log.warn('An error occured while calling getAdResource', err)
      }
    }
    return ret
  },

  /**
 * Returns ad's campaign
 *
 * @memberof youbora.Plugin.prototype
 */
  getAdCampaign: function () {
    if (this.options['ad.campaign']) {
      return this.options['ad.campaign']
    }
    return null
  },

  /**
   * Returns ads adapter getVersion or null
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdAdapterVersion: function () {
    var ret = null
    if (this._adsAdapter) {
      try {
        ret = this._adsAdapter.getVersion()
      } catch (err) {
        Log.warn('An error occured while calling getAdsAdapterVersion', err)
      }
    }
    return ret
  },

  /**
   * Returns ad's AdMetadata
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdMetadata: function () {
    return this.options['ad.metadata']
  },

  // ----------------------------------------- CHRONOS ------------------------------------------

  /**
   * Returns AdJoinDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdJoinDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.join.getDeltaTime(false) : -1
  },

  /**
   * Returns AdBufferDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdBufferDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.buffer.getDeltaTime(false) : -1
  },

  /**
   * Returns AdPauseDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdPauseDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.pause.getDeltaTime(false) : 0
  },

  /**
   * Returns total totalAdDuration chrono delta time
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdTotalDuration: function () {
    return this._adsAdapter ? this._adsAdapter.chronos.total.getDeltaTime(false) : -1
  }
}

module.exports = PluginAdsGettersMixin
