var Util = require('../util')

var version = require('../version')

// This file is designed to add extra functionalities to Plugin class

var PluginGetterMixin = {
  /**
   * Returns service host
   *
   * @memberof youbora.Plugin.prototype
   */
  getHost: function () {
    var host = this.options['host']
    if (this.viewTransform && this.viewTransform.response && this.viewTransform.response.host) {
      host = this.viewTransform.response.host
    }
    return Util.addProtocol(Util.stripProtocol(host), this.options['httpSecure'])
  },


  getUserType: function () {
    return this.options['userType']
  },

  /**
   * Returns parse HLS Flag
   *
   * @memberof youbora.Plugin.prototype
   */
  isParseHls: function () {
    return this.options['parse.hls']
  },

  /**
   * Returns parse CdnNode Flag
   *
   * @memberof youbora.Plugin.prototype
   */
  isParseCdnNode: function () {
    return this.options['parse.cdnNode']
  },

  /**
 * Returns parse location header
 *
 * @memberof youbora.Plugin.prototype
 */
  isLocHeader: function () {
    return this.options['parse.locationHeader']
  },

  /**
   * Returns Cdn list
   *
   * @memberof youbora.Plugin.prototype
   */
  getParseCdnNodeList: function () {
    return this.options['parse.cdnNode.list']
  },

  /**
   * Returns Cdn header name
   *
   * @memberof youbora.Plugin.prototype
   */
  getParseCdnNodeNameHeader: function () {
    return this.options['parse.cdnNameHeader']
  },

  /**
  * Returns obfuscateIp option
  *
  * @memberof youbora.Plugin.prototype
  */
  getObfuscateIp: function () {
    return this.options['obfuscateIp']
  },

  /**
   * Returns content's Extraparam1
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam1: function () {
    return this.options['extraparam.1']
  },

  /**
   * Returns content's Extraparam2
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam2: function () {
    return this.options['extraparam.2']
  },

  /**
   * Returns content's Extraparam3
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam3: function () {
    return this.options['extraparam.3']
  },

  /**
   * Returns content's Extraparam4
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam4: function () {
    return this.options['extraparam.4']
  },
  /**
   * Returns content's Extraparam5
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam5: function () {
    return this.options['extraparam.5']
  },

  /**
   * Returns content's Extraparam6
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam6: function () {
    return this.options['extraparam.6']
  },

  /**
   * Returns content's Extraparam7
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam7: function () {
    return this.options['extraparam.7']
  },

  /**
   * Returns content's Extraparam8
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam8: function () {
    return this.options['extraparam.8']
  },

  /**
   * Returns content's Extraparam9
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam9: function () {
    return this.options['extraparam.9']
  },

  /**
   * Returns content's Extraparam10
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam10: function () {
    return this.options['extraparam.10']
  },

  /**
 * Returns content's Extraparam11
 *
 * @memberof youbora.Plugin.prototype
 */
  getExtraparam11: function () {
    return this.options['extraparam.11']
  },

  /**
   * Returns content's Extraparam12
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam12: function () {
    return this.options['extraparam.12']
  },

  /**
   * Returns content's Extraparam13
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam13: function () {
    return this.options['extraparam.13']
  },

  /**
   * Returns content's Extraparam14
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam14: function () {
    return this.options['extraparam.14']
  },
  /**
   * Returns content's Extraparam15
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam15: function () {
    return this.options['extraparam.15']
  },

  /**
   * Returns content's Extraparam16
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam16: function () {
    return this.options['extraparam.16']
  },

  /**
   * Returns content's Extraparam17
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam17: function () {
    return this.options['extraparam.17']
  },

  /**
   * Returns content's Extraparam18
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam18: function () {
    return this.options['extraparam.18']
  },

  /**
   * Returns content's Extraparam19
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam19: function () {
    return this.options['extraparam.19']
  },

  /**
   * Returns content's Extraparam20
   *
   * @memberof youbora.Plugin.prototype
   */
  getExtraparam20: function () {
    return this.options['extraparam.20']
  },

  /**
   * Returns ad's Extraparam1
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam1: function () {
    return this.options['ad.extraparam.1']
  },

  /**
   * Returns ad's Extraparam2
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam2: function () {
    return this.options['ad.extraparam.2']
  },

  /**
   * Returns ad's Extraparam3
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam3: function () {
    return this.options['ad.extraparam.3']
  },

  /**
   * Returns ad's Extraparam4
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam4: function () {
    return this.options['ad.extraparam.4']
  },
  /**
   * Returns ad's Extraparam5
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam5: function () {
    return this.options['ad.extraparam.5']
  },

  /**
   * Returns ad's Extraparam6
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam6: function () {
    return this.options['ad.extraparam.6']
  },

  /**
   * Returns ad's Extraparam7
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam7: function () {
    return this.options['ad.extraparam.7']
  },

  /**
   * Returns ad's Extraparam8
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam8: function () {
    return this.options['ad.extraparam.8']
  },

  /**
   * Returns ad's Extraparam9
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam9: function () {
    return this.options['ad.extraparam.9']
  },

  /**
   * Returns ad's Extraparam10
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdExtraparam10: function () {
    return this.options['ad.extraparam.10']
  },

  /**
   * Returns PluginInfo
   *
   * @memberof youbora.Plugin.prototype
   */
  getPluginInfo: function () {
    var ret = {
      lib: version,
      adapter: this.getAdapterVersion(),
      adAdapter: this.getAdAdapterVersion()
    }
    return ret
  },

  /**
   * Returns Ip
   *
   * @memberof youbora.Plugin.prototype
   */
  getIp: function () {
    return this.options['network.ip']
  },

  /**
   * Returns Isp
   *
   * @memberof youbora.Plugin.prototype
   */
  getIsp: function () {
    return this.options['network.isp']
  },

  /**
   * Returns ConnectionType
   *
   * @memberof youbora.Plugin.prototype
   */
  getConnectionType: function () {
    return this.options['network.connectionType']
  },

  /**
   * Returns DeviceCode
   *
   * @memberof youbora.Plugin.prototype
   */
  getDeviceCode: function () {
    return this.options['device.code']
  },

  /**
   * Returns AccountCode
   *
   * @memberof youbora.Plugin.prototype
   */
  getAccountCode: function () {
    return this.options['accountCode']
  },

  /**
   * Returns Username
   *
   * @memberof youbora.Plugin.prototype
   */
  getUsername: function () {
    return this.options['username']
  },

  /**
   * Get URL referer.
   *
   * @memberof youbora.Plugin.prototype
   */
  getReferer: function () {
    var ret = ''
    if (typeof window !== 'undefined' && window.location) {
      ret = window.location.href
    }
    return ret
  },

  /**
   * Returns the nodehost
   *
   * @memberof youbora.Plugin.prototype
   */
  getNodeHost: function () {
    return this.options['content.cdnNode'] || this.resourceTransform.getNodeHost()
  },

  /**
   * Returns the node type id
   *
   * @memberof youbora.Plugin.prototype
   */
  getNodeType: function () {
    return this.options['content.cdnType'] || this.resourceTransform.getNodeType()
  },

  /**
   * Returns the node type string
   *
   * @memberof youbora.Plugin.prototype
   */
  getNodeTypeString: function () {
    return this.resourceTransform.getNodeTypeString()
  },

  /**
  * Returns requestNumber value, to prevent /data calls being cached
  *
  * @memberof youbora.Plugin.prototype
  */
  getRequestNumber: function () {
    return Math.random()
  },
}

module.exports = PluginGetterMixin
