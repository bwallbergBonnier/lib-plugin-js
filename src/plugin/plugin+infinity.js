var Constants = require('../constants')
var Log = require('../log')
var YBRequest = require('../comm/request')

var YouboraInfinity = require('../infinity/infinity')

// This file is designed to add extra functionalities to Plugin class

var PluginInfinityMixin = {
  _initInfinity: function () {
    this.infinity.on(YouboraInfinity.Event.NAV, this._navListener.bind(this))
    this.infinity.on(YouboraInfinity.Event.SESSION_START, this._sessionStartListener.bind(this))
    this.infinity.on(YouboraInfinity.Event.SESSION_STOP, this._sessionStopListener.bind(this))
    this.infinity.on(YouboraInfinity.Event.EVENT, this._eventListener.bind(this))
  },

  _sendInfinity: function (willSendEvent, service, params) {
    params = this.requestBuilder.buildParams(params, service)

    var data = {
      params: params,
      plugin: this,
      adapter: this.getAdapter(),
      adsAdapter: this.getAdsAdapter()
    }

    this.emit(willSendEvent, data)

    if (this.infinity._comm && params !== null && this.options.enabled) {
      this.lastServeiceSent = service
      this.infinity._comm.sendRequest(new YBRequest(null, service, params))
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------

  _navListener: function (e) {
    var params = e.data.params || {}
    this._sendInfinity(Constants.WillSendEvent.WILL_SEND_NAV, Constants.Service.NAV, params)

    // start beats
    if (!this._beat.isRunning) this._beat.start()

    Log.notice(Constants.Service.NAV + ' ' + params.route)
  },

  _sessionStartListener: function (e) {
    var params = e.data.params || {}
    this._sendInfinity(
      Constants.WillSendEvent.WILL_SEND_SESSION_START,
      Constants.Service.SESSION_START,
      params
    )

    // start beats
    if (!this._beat.isRunning) this._beat.start()

    Log.notice(Constants.Service.SESSION_START + ' ' + params.route)
  },

  _sessionStopListener: function (e) {
    var params = e.data.params || {}
    this._sendInfinity(
      Constants.WillSendEvent.WILL_SEND_SESSION_STOP,
      Constants.Service.SESSION_STOP,
      params
    )

    // stop beats
    if (!this._beat.isRunning) this._beat.stop()

    Log.notice(Constants.Service.SESSION_STOP + ' ' + params.route)
  },

  _eventListener: function (e) {
    var params = e.data.params || {}
    this._sendInfinity(Constants.WillSendEvent.WILL_SEND_EVENT, Constants.Service.EVENT, params)
    Log.notice(Constants.Service.EVENT + ' ' + params.name)
  },

  /**
   * Sends beat request
   *
   * @param {number} diffTime Time since the last ping
   *
   * @private
   * @memberof youbora.Plugin.prototype
   */
  _sendBeat: function (diffTime) {
    var params = {
      diffTime: diffTime,
      sessions: [this.getSessionId()]
    }

    this._sendInfinity(Constants.WillSendEvent.WILL_SEND_BEAT, Constants.Service.BEAT, params)
    Log.verbose(Constants.Service.BEAT)
  }
}

module.exports = PluginInfinityMixin
