var Log = require('../log')
var Constants = require('../constants')

var Adapter = require('../adapter/adapter')

// This file is designed to add extra functionalities to Plugin class

/** @lends youbora.Plugin.prototype */
var PluginContentMixin = {
  /**
   * Sets an adapter for video content.
   *
   * @param {Adapter} adapter
   *
   * @memberof youbora.Plugin.prototype
   */
  setAdapter: function (adapter) {
    if (adapter.plugin) {
      Log.warn('Adapters can only be added to a single plugin')
    } else {
      this.removeAdapter()

      this._adapter = adapter
      adapter.plugin = this

      // Register listeners
      adapter.on(Adapter.Event.INIT, this._initListener.bind(this))
      adapter.on(Adapter.Event.START, this._startListener.bind(this))
      adapter.on(Adapter.Event.JOIN, this._joinListener.bind(this))
      adapter.on(Adapter.Event.PAUSE, this._pauseListener.bind(this))
      adapter.on(Adapter.Event.RESUME, this._resumeListener.bind(this))
      adapter.on(Adapter.Event.SEEK_BEGIN, this._seekBeginListener.bind(this))
      adapter.on(Adapter.Event.SEEK_END, this._seekEndListener.bind(this))
      adapter.on(Adapter.Event.BUFFER_BEGIN, this._bufferBeginListener.bind(this))
      adapter.on(Adapter.Event.BUFFER_END, this._bufferEndListener.bind(this))
      adapter.on(Adapter.Event.ERROR, this._errorListener.bind(this))
      adapter.on(Adapter.Event.STOP, this._stopListener.bind(this))
      adapter.on(Adapter.Event.CLICK, this._clickListener.bind(this))
      adapter.on(Adapter.Event.BLOCKED, this._blockedListener.bind(this))
    }
  },

  /**
   * Returns current adapter or null.
   *
   * @returns {Adapter}
   *
   * @memberof youbora.Plugin.prototype
   */
  getAdapter: function () {
    return this._adapter
  },

  /**
   * Removes the current adapter. Fires stop if needed. Calls adapter.dispose().
   *
   * @memberof youbora.Plugin.prototype
   * */
  removeAdapter: function () {
    if (this._adapter) {
      this._adapter.dispose()

      this._adapter.plugin = null

      this._adapter.off(Adapter.Event.INIT, this._initListener)
      this._adapter.off(Adapter.Event.START, this._startListener)
      this._adapter.off(Adapter.Event.JOIN, this._joinListener)
      this._adapter.off(Adapter.Event.PAUSE, this._pauseListener)
      this._adapter.off(Adapter.Event.RESUME, this._resumeListener)
      this._adapter.off(Adapter.Event.SEEK_BEGIN, this._seekBeginListener)
      this._adapter.off(Adapter.Event.SEEK_END, this._seekEndListener)
      this._adapter.off(Adapter.Event.BUFFER_BEGIN, this._bufferBeginListener)
      this._adapter.off(Adapter.Event.BUFFER_END, this._bufferEndListener)
      this._adapter.off(Adapter.Event.ERROR, this._errorListener)
      this._adapter.off(Adapter.Event.STOP, this._stopListener)

      this._adapter = null
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------
  _initListener: function (e) {
    if (!this._adapter || !this._adapter.flags.isInited) {
      if (!this.isInitiated || this.lastServeiceSent === Constants.Service.ERROR) {
        this.viewTransform.nextView()
        this._initComm()
        this._startPings()
      }
      var params = e.data.params || {}
      this.isInitiated = true
      if (this._adapter) {
        this._adapter.flags.isInited = true
        this._adapter.chronos.join.start()
      } else {
        this.initChrono.start()
      }
      this._send(Constants.WillSendEvent.WILL_SEND_INIT, Constants.Service.INIT, params)
      Log.notice(Constants.Service.INIT + ' ' + (params.title || params.mediaResource))
    }
  },

  _startListener: function (e) {
    if (!this.isInitiated) {
      this.viewTransform.nextView()
      this._initComm()
      this._startPings()
    }
    var params = e.data.params || {}
    var allParamsReady = (!!this.options['content.resource'] || !!this._adapter.getResource()) &&
      (typeof this.options["content.isLive"] === 'boolean' || typeof this._adapter.getIsLive() === 'boolean') &&
      (typeof this.options['content.duration'] === 'number' || typeof this._adapter.getDuration() === 'number' || this._adapter.getIsLive() || this.options["content.isLive"]) &&
      (!!this.options['content.title'] || !!this._adapter.getTitle())
    if (allParamsReady && !this._adapter.flags.isInited && !this.isInitiated) { //start
      this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, params)
      Log.notice(Constants.Service.START + ' ' + (params.title || params.mediaResource))
      this.isStarted = true
      //chrono if had no adapter when inited
      if (this.isInitiated && this.initChrono.startTime != 0) {
        this._adapter.chronos.join.startTime = this.initChrono.startTime
      }
    } else if (!this._adapter.flags.isInited && !this.isInitiated) {//init
      this.isInitiated = true
      this._adapter.flags.isInited = true
      this._adapter.chronos.join.start()
      this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.INIT, params)
      Log.notice(Constants.Service.INIT + ' ' + (params.title || params.mediaResource))
    }
  },

  _joinListener: function (e) {
    var params = e.data.params || {}
    if (!this._adsAdapter || !this._adsAdapter.flags.isStarted) {
      if (this.isInitiated && !this.isStarted) { //start if just inited
        //chrono if had no adapter when inited
        if (this.initChrono.startTime != 0) {
          this._adapter.chronos.join.startTime = this.initChrono.startTime
        }
        this._send(Constants.WillSendEvent.WILL_SEND_START, Constants.Service.START, params)
        Log.notice(Constants.Service.START + ' ' + (params.title || params.mediaResource))
        this.isStarted = true
      }
      params = e.data.params || {}
      this._send(Constants.WillSendEvent.WILL_SEND_JOIN, Constants.Service.JOIN, params)
      Log.notice(Constants.Service.JOIN + ' ' + params.joinDuration + 'ms')
    } else { // If it is currently showing ads, join is invalidated
      if (this._adapter.monitor) this._adapter.monitor.stop()
      this._adapter.flags.isJoined = false
      this._adapter.chronos.join.stopTime = 0
    }
  },

  _pauseListener: function (e) {
    if (this._adapter) {
      if (this._adapter.flags.isBuffering ||
        this._adapter.flags.isSeeking ||
        (this._adsAdapter && this._adsAdapter.flags.isStarted)) {
        this._adapter.chronos.pause.reset()
      }
    }

    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_PAUSE, Constants.Service.PAUSE, params)
    Log.notice(Constants.Service.PAUSE + ' at ' + params.playhead + 's')
  },

  _resumeListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_RESUME, Constants.Service.RESUME, params)
    Log.notice(Constants.Service.RESUME + ' ' + params.pauseDuration + 'ms')
  },

  _seekBeginListener: function (e) {
    if (this._adapter && this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    Log.notice('Seek Begin')
  },

  _seekEndListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_SEEK, Constants.Service.SEEK, params)
    Log.notice(Constants.Service.SEEK +
      ' to ' + params.playhead +
      ' in ' + params.seekDuration + 'ms'
    )
  },

  _bufferBeginListener: function (e) {
    if (this._adapter && this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    Log.notice('Buffer Begin')
  },

  _bufferEndListener: function (e) {
    var params = e.data.params || {}
    this._send(Constants.WillSendEvent.WILL_SEND_BUFFER, Constants.Service.BUFFER, params)
    Log.notice(Constants.Service.BUFFER +
      ' to ' + params.playhead +
      ' in ' + params.bufferDuration + 'ms'
    )
  },

  _errorListener: function (e) {
    this.fireError(e.data.params || {})
  },

  _stopListener: function (e) {
    this.fireStop(e.data.params || {})
  },

  _clickListener: function (e) {
    Log.warn(
      'You have sent a CLICK event from the content adapter. ' +
      'Such events must be fired from ads adapters, so it will be ignored.'
    )
  },

  _blockedListener: function (e) {
    Log.warn(
      'You have sent a BLOCKED event from the content adapter. ' +
      'Such events must be fired from ads adapters, so it will be ignored.'
    )
  }
}

module.exports = PluginContentMixin
