var Emitter = require('../emitter')
var Timer = require('../timer')
var Chrono = require('../chrono')
var Constants = require('../constants')
var Util = require('../util')

var YBRequest = require('../comm/request')
var Communication = require('../comm/communication')
var FlowTransform = require('../comm/transform/flow')
var ViewTransform = require('../comm/transform/view')
var ResourceTransform = require('../comm/transform/resource')

var Options = require('./options')
var YouboraStorage = require('./storage')
var RequestBuilder = require('./requestbuilder')

var YouboraInfinity = require('../infinity/infinity')

var Plugin = Emitter.extend(
  /** @lends youbora.Plugin.prototype */
  {
    /**
     * This is the main class of video analytics. You may want to have one instance for each video
     * you want to track. Will need {@link Adapter}s for both content and ads.
     *
     * @constructs Plugin
     * @extends youbora.Emitter
     * @memberof youbora
     *
     * @param {Options} [options] An object complying with {@link Options} constructor.
     * @param {Adapter} [adapter] If an adapter is provided, setAdapter will be immediately called.
     */
    constructor: function (options, adapter) {
      /** Instance of youbora infinity. */
      this.infinity = new YouboraInfinity(this)

      /** This flags indicates that /init has been called. */
      this.isInitiated = false

      /** This flags indicate that the content is preloading. */
      this.isPreloading = false

      /** Chrono for preload times. */
      this.preloadChrono = new Chrono()
      this.initChrono = new Chrono()

      /** Reference to {@link youbora.YouboraStorage} */
      this.storage = new YouboraStorage()

      /** Stored {@link Options} of the session. */
      this.options = new Options(options)
      this._adapter = null
      this._adsAdapter = null
      this._ping = new Timer(this._sendPing.bind(this), 5000)
      this._beat = new Timer(this._sendBeat.bind(this), 30000)

      this.requestBuilder = new RequestBuilder(this)

      this.resourceTransform = new ResourceTransform(this)

      this.lastEventTime = null

      if (adapter) this.setAdapter(adapter)

      // FastData
      this.viewTransform = new ViewTransform(this)
      this.viewTransform.on(ViewTransform.Event.DONE, this._receiveData.bind(this))

      if (this.getIsDataExpired()) {
        this.viewTransform.init() // request a new data
      } else {
        this.viewTransform.setData(this.getStoredData()) // use stored data
      }

      this._initInfinity()
    },

    /**
     * This callback is called when a correct data response is received.
     *
     * @param {any} e Response from fastdata
     */
    _receiveData: function (e) {
      this._ping.interval = e.target.response.pingTime * 1000
      this._beat.interval = e.target.response.beatTime * 1000
      this.storage.setSession('data', e.target.response.msg)

      if (this.getIsSessionExpired()) {
        this.viewTransform.setSessionId(this.viewTransform.response.code)
        this.storage.setLocal('session', this.viewTransform.response.code)
      } else {
        this.viewTransform.setSessionId(this.getSessionId())
      }
    },

    /**
     * Reset all variables and stop all timers
     * @private
     */
    _reset: function () {
      this._stopPings()
      this.resourceTransform = new ResourceTransform(this)

      this.isInitiated = false
      this.isPreloading = false
      this.preloadChrono.reset()
      this.initChrono.reset()
    },

    /**
     * Creates and enqueues related request using {@link Communication#sendRequest}.
     * It will fire will-send-events.
     *
     * @param {string} willSendEvent Name of the will-send event. Use {@link Plugin.Event} enum.
     * @param {string} service Name of the service. Use {@link Constants.Service} enum.
     * @param {Object} params Params of the request
     * @private
     */
    _send: function (willSendEvent, service, params) {
      if (this.getIsLive()) {
        params.mediaDuration = undefined
      }
      var now = new Date().getTime()
      if (this.lastEventTime && (now > (this.lastEventTime + 600000))) { // 600000ms = 10 minutes
        // if last event was sent more than 10 minutes ago, it will use new view code
        this.viewTransform.nextView()
      }
      this.lastEventTime = now
      if (service === Constants.Service.STOP) {
        this.lastEventTime = null
      }

      params = this.requestBuilder.buildParams(params, service)

      var data = {
        params: params,
        plugin: this,
        adapter: this.getAdapter(),
        adsAdapter: this.getAdsAdapter()
      }

      this.emit(willSendEvent, data)

      if (this._comm && params !== null && this.options.enabled) {
        this.lastServeiceSent = service
        this._comm.sendRequest(new YBRequest(null, service, params))
      }
    },

    /**
     * Initializes comm and its transforms.
     * @private
     */
    _initComm: function () {
      this.resourceTransform.init(this.getResource())

      this._comm = new Communication()
      this._comm.addTransform(new FlowTransform())
      this._comm.addTransform(this.viewTransform)
      this._comm.addTransform(this.resourceTransform)
      // this._comm.addTransform(new Nqs6Transform())
    },

    /**
     * Returns the current {@link youbora.Communication} instance.
     *
     * @returns {youbora.Communication} communication instance
     */
    getComm: function () {
      return this._comm
    },

    /**
     * Modifies current options. See {@link Options.setOptions}.
     *
     * @param {any} options
     */
    setOptions: function (options) {
      this.options.setOptions(options)
    },

    /**
     * Disable request sending.
     */
    disable: function () {
      this.setOptions({ 'enabled': false })
    },

    /**
     * Re-enable request sending.
     */
    enable: function () {
      this.setOptions({ 'enabled': true })
    }
  },

  /** @lends youbora.Plugin */
  {
    // Static Memebers //
    /**
     * List of events that could be fired
     * @enum
     * @event
     */
    Event: Constants.WillSendEvent
  }
)

// Apply Mixins
// Plugin is actually a big class, I decided to separate the logic into
// different mixin files to ease the maintainability of each file.
// Filename convention will be plugin+xxxxx.js where xxxxx is the added functionality.
Util.assign(Plugin.prototype, require('./plugin+content'))
Util.assign(Plugin.prototype, require('./plugin+getters'))
Util.assign(Plugin.prototype, require('./plugin+content+getters'))
Util.assign(Plugin.prototype, require('./plugin+ads'))
Util.assign(Plugin.prototype, require('./plugin+ads+getters'))
Util.assign(Plugin.prototype, require('./plugin+pings'))
Util.assign(Plugin.prototype, require('./plugin+fire'))
Util.assign(Plugin.prototype, require('./plugin+infinity'))
Util.assign(Plugin.prototype, require('./plugin+infinity+getters'))

module.exports = Plugin
