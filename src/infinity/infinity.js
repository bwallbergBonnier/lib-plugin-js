var Emitter = require('../emitter')
var Comm = require('../comm/communication')

var YouboraInfinity = Emitter.extend(
  /** @lends youbora.Infinity.prototype */
  {

    /**
     * This class is the base of youbora infinity. Every plugin will have an instance.
     *
     * @param {youbora.Plugin} plugin Parent plugin.
     *
     * @constructs youbora.Infinity
     * @extends youbora.Emitter
     * @memberof youbora
     */
    constructor: function (plugin) {
      /** Parent {@link youbora.Plugin} reference. */
      this._plugin = plugin
    },

    /**
     * @alias youbora.Infinity.prototype.begin.
     */
    andBeyond: function () {
      YouboraInfinity.prototype.begin.apply(this, arguments)
    },

    /**
     * This method will start infinity logic, setting storage as needed.
     * Will call fireSessionStart the first time and fireNav for every subsequent route change.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    begin: function (params) {
      this._comm = new Comm()
      this._comm.addTransform(this._plugin.viewTransform)

      if (this._plugin.getContext()) {
        this.fireNav(params) // returning
      } else {
        this.fireSessionStart(params) // first time
      }
    },

    _generateNewContext: function () {
      var context = btoa(new Date().getTime())
      this._plugin.storage.setSession('context', context)
    },

    _setLastActive: function () {
      this._plugin.storage.setSession('lastactive', new Date().getTime())
    },

    /**
     * Returns the current {@link youbora.Communication} instance.
     *
     * @returns {youbora.Communication} communication instance
     */
    getComm: function () {
      return this._comm
    },

    // Fire
    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireSessionStart: function (params) {
      this._generateNewContext()
      this.emit(YouboraInfinity.Event.SESSION_START, { params: params })
      this._setLastActive()
    },

    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireSessionStop: function (params) {
      this.emit(YouboraInfinity.Event.SESSION_STOP, { params: params })
      this._setLastActive()
    },

    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireNav: function (params) {
      this.emit(YouboraInfinity.Event.NAV, { params: params })
      this._setLastActive()
    },

    /**
     * Emits session start request.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireEvent: function (params) {
      this.emit(YouboraInfinity.Event.EVENT, { params: params })
      this._setLastActive()
    }
  },
  /** @lends youbora.Plugin */
  {
    // Static Memebers //
    /**
     * List of events that could be fired
     * @enum
     * @event
     */
    Event: {
      NAV: 'nav',
      SESSION_START: 'sessionStart',
      SESSION_STOP: 'sessionStop',
      BEAT: 'beat',
      EVENT: 'event'
    }
  }
)

module.exports = YouboraInfinity
