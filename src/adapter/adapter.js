var Emitter = require('../emitter')
var Log = require('../log')
var Util = require('../util')
var HybridNetwork = require('../hybridnetwork')
var version = require('../version')
var PlaybackChronos = require('./playbackchronos')
var PlaybackFlags = require('./playbackflags')
var PlayheadMonitor = require('./playheadmonitor')

var Adapter = Emitter.extend(
  /** @lends youbora.Adapter.prototype */
  {
    /**
     * Main Adapter class. All specific player adapters should extend this class specifying a player
     * class.
     *
     * The Adapter works as the 'glue' between the player and YOUBORA acting both as event
     * translator and as proxy for the {@link Plugin} to get info from the player.
     *
     * @constructs Adapter
     * @extends youbora.Emitter
     * @memberof youbora
     *
     * @param {object|string} player Either an instance of the player or a string containing an ID.
     */
    constructor: function (player) {
      /** An instance of {@link FlagStatus} */
      this.flags = new PlaybackFlags()

      /** An instance of {@link ChronoStatus} */
      this.chronos = new PlaybackChronos()

      /** Reference to {@link PlayheadMonitor}. Helps the plugin detect buffers/seeks. */
      this.monitor = null

      /** Reference to {@link Plugin}. */
      this.plugin = null

      /** Reference to the player tag */
      this.player = null

      // Register player and event listeners
      this.setPlayer(player)

      /** Reference to the video/object tag, could be the same as the player. */
      this.tag = this.player

      Log.notice('Adapter ' + this.getVersion() + ' with Lib ' + version + ' is ready.')
    },

    /**
     * Sets a new player, removes the old listeners if needed.
     *
     * @param {Object} player Player to be registered.
     */
    setPlayer: function (player) {
      if (this.player) this.unregisterListeners()

      if (typeof player === 'string') {
        this.player = document.getElementById(player)
      } else {
        this.player = player
      }

      if (this.player) this.registerListeners()
    },

    /**
     * Override to create event binders.
     * It's a good practice when implementing a new Adapter to create intermediate methods and call
     * those when player events are detected instead of just calling the `fire*` methods. This
     * will allow future users of the Adapter to customize its behaviour by overriding these
     * methods.
     *
     * @example
     * registerListeners: function () {
     *  this.player.addEventListener('start', this.onStart.bind(this))
     * },
     *
     * onStart: function (e) {
     *  this.emit('start')
     * }
     */
    registerListeners: function () { },

    /**
     * Override to create event de-binders.
     *
     * @example
     * registerListeners: function () {
     *  this.player.removeEventListener('start', this.onStart)
     * }
     */
    unregisterListeners: function () { },

    /**
     * This function disposes the currend adapter, removes player listeners and drops references.
     */
    dispose: function () {
      if (this.monitor) this.monitor.stop()
      this.fireStop()
      this.unregisterListeners()
      this.player = null
      this.tag = null
    },

    /**
     * Creates a new {@link PlayheadMonitor} at this.monitor.
     *
     * @param {bool} monitorBuffers If true, it will monitor buffers.
     * @param {bool} monitorSeeks If true, it will monitor seeks.
     * @param {number} [interval=800] The interval time in ms.
     */
    monitorPlayhead: function (monitorBuffers, monitorSeeks, interval) {
      var type = 0
      if (monitorBuffers) type |= PlayheadMonitor.Type.BUFFER
      if (monitorSeeks) type |= PlayheadMonitor.Type.SEEK

      this.monitor = new PlayheadMonitor(this, type, interval)
    },

    // GETTERS //

    /** Override to return current playhead of the video */
    getPlayhead: function () {
      return null
    },

    /** Override to return current playrate */
    getPlayrate: function () {
      return !this.flags.isPaused ? 1 : 0
    },

    /** Override to return Frames Per Secon (FPS) */
    getFramesPerSecond: function () {
      return null
    },

    /** Override to return dropped frames since start */
    getDroppedFrames: function () {
      return null
    },

    /** Override to return video duration */
    getDuration: function () {
      return null
    },

    /** Override to return current bitrate */
    getBitrate: function () {
      return null
    },

    /** Override to return user bandwidth throughput */
    getThroughput: function () {
      if (this.getCdnTraffic() && this.getCdnTraffic() !== 0) {
        if (!this.lastDataValue) {
          this.lastDataValue = 0
        }
        var prevDataValue = this.lastDataValue
        this.lastDataValue = this.getCdnTraffic() + this.getP2PTraffic()
        return Math.round((this.lastDataValue - prevDataValue) / (this.plugin._ping.interval / 1000))
      }
      return null
    },

    /** Override to return rendition */
    getRendition: function () {
      return null
    },

    /** Override to return title */
    getTitle: function () {
      return null
    },

    /** Override to return title2 */
    getTitle2: function () {
      return null
    },

    /** Override to recurn true if live and false if VOD */
    getIsLive: function () {
      return null
    },

    /** Override to return resource URL. */
    getResource: function () {
      return null
    },

    /** Override to return player version */
    getPlayerVersion: function () {
      return null
    },

    /** Override to return player's name */
    getPlayerName: function () {
      return null
    },

    /** Override to return adapter version. */
    getVersion: function () {
      return version + '-generic-js'
    },

    /** Override to return current ad position (only ads) */
    getPosition: function () {
      return null
    },

    /** Override to return CDN traffic bytes not using streamroot or peer5. */
    getCdnTraffic: function () {
      return HybridNetwork.getCdnTraffic()
    },

    /** Override to return P2P traffic bytes not using streamroot or peer5. */
    getP2PTraffic: function () {
      return HybridNetwork.getP2PTraffic()
    },

    /** Override to return P2P traffic sent in bytes, not using streamroot or peer5. */
    getUploadTraffic: function () {
      return HybridNetwork.getUploadTraffic()
    },

    /** Override to return if P2P is enabled not using streamroot or peer5. */
    getIsP2PEnabled: function () {
      return HybridNetwork.getIsP2PEnabled()
    },

    /** Override to return household id */
    getHouseholdId: function () {
      return null
    },

    // FLOW //

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent init if isStarted or isInited are already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireInit: function (params) {
      if (!this.flags.isStarted && !this.flags.isInited) {

        this.chronos.total.start()
        this.chronos.join.start()

        this.emit(Adapter.Event.INIT, { params: params })
        this.flags.isInited = true
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireStart: function (params) {
      if (!this.flags.isStarted) {
        this.flags.isStarted = true

        if (!this.flags.isInited) {
          this.chronos.total.start()
          this.chronos.join.start()
        }
        this.emit(Adapter.Event.START, { params: params })
        this.flags.isInited = false
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireJoin: function (params) {
      if ((this.flags.isStarted || this.flags.isInited) && !this.flags.isJoined) {
        this.flags.isStarted = true
        if (this.monitor) this.monitor.start()

        this.flags.isJoined = true

        this.chronos.join.stop()

        this.emit(Adapter.Event.JOIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    firePause: function (params) {
      if (this.flags.isJoined && !this.flags.isPaused) {
        this.flags.isPaused = true

        this.chronos.pause.start()

        this.emit(Adapter.Event.PAUSE, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireResume: function (params) {
      if (this.flags.isJoined && this.flags.isPaused) {
        this.flags.isPaused = false

        this.chronos.pause.stop()

        if (this.monitor) this.monitor.skipNextTick()

        this.emit(Adapter.Event.RESUME, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {bool} [convertFromSeek=false] If true, will convert current seek to buffer.
     */
    fireBufferBegin: function (params, convertFromSeek) {
      if (this.flags.isJoined && !this.flags.isBuffering) {
        if (this.flags.isSeeking) {
          if (convertFromSeek) {
            Log.notice('Converting current buffer to seek')

            this.chronos.buffer = this.chronos.seek.clone()
            this.chronos.seek.reset()

            this.flags.isSeeking = false
          } else {
            return
          }
        } else {
          this.chronos.buffer.start()
        }

        this.flags.isBuffering = true
        this.emit(Adapter.Event.BUFFER_BEGIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireBufferEnd: function (params) {
      if (this.flags.isJoined && this.flags.isBuffering) {
        this.flags.isBuffering = false

        this.chronos.buffer.stop()

        if (this.monitor) this.monitor.skipNextTick()

        this.emit(Adapter.Event.BUFFER_END, { params: params })
      }
    },

    /**
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    cancelBuffer: function (params) {
      if (this.flags.isJoined && this.flags.isBuffering) {
        this.flags.isBuffering = false

        this.chronos.buffer.stop()

        if (this.monitor) this.monitor.skipNextTick()

      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     * @param {bool} [convertFromBuffer=true] If false, will convert current buffer to seek.
     */
    fireSeekBegin: function (params, convertFromBuffer) {
      if (this.flags.isJoined && !this.flags.isSeeking) {
        if (this.flags.isBuffering) {
          if (convertFromBuffer !== false) {
            Log.notice('Converting current buffer to seek')

            this.chronos.seek = this.chronos.buffer.clone()
            this.chronos.buffer.reset()

            this.flags.isBuffering = false
          } else {
            return
          }
        } else {
          this.chronos.seek.start()
        }

        this.flags.isSeeking = true
        this.emit(Adapter.Event.SEEK_BEGIN, { params: params })
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireSeekEnd: function (params) {
      if (this.flags.isJoined && this.flags.isSeeking) {
        this.flags.isSeeking = false

        this.chronos.seek.stop()

        if (this.monitor) this.monitor.skipNextTick()

        this.emit(Adapter.Event.SEEK_END, { params: params })
      }
    },

    /**
   *
   * @param {Object} [params] Object of key:value params to add to the request.
   */
    cancelSeek: function (params) {
      if (this.flags.isJoined && this.flags.isSeeking) {
        this.flags.isSeeking = false

        this.chronos.seek.stop()

        if (this.monitor) this.monitor.skipNextTick()
      }
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireStop: function (params) {
      if (this.flags.isStarted) {
        if (this.monitor) this.monitor.stop()

        this.flags.reset()

        this.chronos.total.stop()
        this.chronos.join.reset()
        this.chronos.pause.reset()
        this.chronos.buffer.reset()
        this.chronos.seek.reset()

        this.emit(Adapter.Event.STOP, { params: params })

        if (this.plugin._adapter.flags.isEnded && !this.plugin._adapter.flags.isStopped) {
          if (this.plugin.options['ad.afterStop'] === 0) {
            this.plugin.fireStop()
          } else {
            this.plugin.options['ad.afterStop']--
          }
        }
      }
    },

    /**
    * @param {Object} [params] Object of key:value params to add to the request.
    */
    fireCasted: function (params) {
      if (!params) params = {}
      params.casted = true
      this.fireStop(params)
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
     * @param {String} [msg] Error Message
     * @param {Object} [metadata] Object defining error metadata
     * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
     */
    fireError: function (code, msg, metadata, level) {
      var params = Util.buildErrorParams(code, msg, metadata, level)
      this.emit(Adapter.Event.ERROR, { params: params })
    },

    /**
    * ONLY ADS.
    * Emits related event and set flags if current status is valid.
    * ie: won't sent start if isStarted is already true.
    *
    * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
    * @param {String} [msg] Error Message
    * @param {Object} [metadata] Object defining error metadata
    * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
    */
    fireNoServedAd: function (code, msg, metadata, level) {
      var params = Util.buildErrorParams(code, msg, metadata, level)
      params.errorSeverity = "AdsNotServed"
      this.emit(Adapter.Event.ERROR, { params: params })
    },

    /**
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
     * @param {String} [msg] Error Message
     * @param {Object} [metadata] Object defining error metadata
     */
    fireFatalError: function (code, msg, metadata) {
      if (this.monitor) this.monitor.stop()

      this.fireError(code, msg, metadata, 'fatal')
      this.fireStop()
    },

    /**
     * ONLY ADS.
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireClick: function (params) {
      this.emit(Adapter.Event.CLICK, { params: params })
    },

    /**
     * ONLY ADS.
     * Emits related event and set flags if current status is valid.
     * ie: won't sent start if isStarted is already true.
     *
     * @param {Object} [params] Object of key:value params to add to the request.
     */
    fireBlocked: function (params) {
      this.emit(Adapter.Event.BLOCKED, { params: params })
    }
  },

  /** @lends youbora.Adapter */
  {
    // Static Memebers //

    /**
     * List of events that could be fired
     * @enum
     * @event
     */
    Event: {
      START: 'start',
      JOIN: 'join',
      PAUSE: 'pause',
      RESUME: 'resume',
      SEEK_BEGIN: 'seek-begin',
      SEEK_END: 'seek-end',
      BUFFER_BEGIN: 'buffer-begin',
      BUFFER_END: 'buffer-end',
      ERROR: 'error',
      STOP: 'stop',
      CLICK: 'click',
      BLOCKED: 'blocked'
    }
  }
)

module.exports = Adapter
