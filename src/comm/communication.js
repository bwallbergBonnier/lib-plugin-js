var YouboraObject = require('../object')
var Log = require('../log')
var YBRequest = require('./request')
var Transform = require('./transform/transform')

var Communication = YouboraObject.extend(
  /** @lends youbora.Communication.prototype */
  {
    /**
     * Youbora Communication implements an abstraction layer over API requests.
     * Internally, Communication implements queues of {@link Request} objects.
     * This queue can be blocked using {@link Transform}
     *
     * @constructs Communication
     * @extends youbora.YouboraObject
     * @memberof youbora
     *
     * @param {string} host The fastdata host address.
     * @param {boolean} httpSecure True for https, false for http, undefined for //.
     */
    constructor: function () {
      /** Array of {@link Transform}, only when the array is empty the request Queues will begin sending. */
      this.transforms = []

      /**
       * Queue of {@link YBRequest}
       * @private
       */
      this._requests = []
    },

    /**
     * Enqueues the request provided.
     *
     * @param {YBRequest} request Request to be enqueued
     * @param {function} [callback] The defined load callback to the Request
     */
    sendRequest: function (request, callback) {
      if (request) {
        if (typeof callback === 'function') request.on(YBRequest.Event.SUCCESS, callback)
        this._registerRequest(request)
      }
    },

    /**
     * Build a generic request to the given host.
     *
     * @param {string} host Host of the service called.
     * @param {string} service A string with the service to be called. ie: '/data', '/joinTime'...
     * @param {Object} [params] Object of key:value params.
     * @param {function} [callback] The defined load callback to the Request
     */
    buildRequest: function (host, service, params, callback) {
      params = params || {}
      var request = new YBRequest(host, service, params)
      if (typeof callback === 'function') request.on(YBRequest.Event.SUCCESS, callback)
      this._registerRequest(request)
    },

    /**
     * Adds a Transform to the queue. See {@link Transform}.
     *
     * @param {RequestTransform} transform
     */
    addTransform: function (transform) {
      if (transform.parse && transform.isBlocking) {
        this.transforms.push(transform)
        transform.on(Transform.Event.DONE, this._processRequests.bind(this))
      } else {
        Log.warn(transform + ' is not a valid RequestTransform.')
      }
    },

    /**
     * Removes a {@link Transform}.
     *
     * @param {RequestTransform} transform Transform object to remove.
     */
    removeTransform: function (transform) {
      var pos = this.transforms.indexOf(transform)
      if (pos !== -1) {
        this.transforms.splice(pos, 1)
      } else {
        Log.warn('Trying to remove unexisting Transform \'' + transform + '\'.')
      }
    },

    /**
     * Adds an {@link YBRequest} to the queue of requests.
     *
     * @private
     * @param {YBRequest} request The Request to be queued.
     */
    _registerRequest: function (request) {
      this._requests.push(request)
      this._processRequests()
    },

    /**
     * Execute pending requests in the queue. Returns rejected ones to the queue.
     * @private
     */
    _processRequests: function () {
      var workingQueue = this._requests
      this._requests = []

      var rejected = []
      while (workingQueue.length) {
        var request = workingQueue.shift()
        if (this._transform(request)) {
          request.send()
        } else {
          rejected.push(request)
        }
      }

      while (rejected.length) {
        this._requests.push(rejected.shift())
      }
    },

    /**
     * Pass the given request to each transform.
     * @private
     * @returns {bool} True if everything is right. False if some parser rejected it.
     */
    _transform: function (request) {
      var ret = true
      this.transforms.forEach(function (transform) {
        if (transform.isBlocking(request)) {
          ret = false
          return // break foreach loop
        } else {
          transform.parse(request)
        }
      })
      return ret
    }
  })

module.exports = Communication
