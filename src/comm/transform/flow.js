var Transform = require('./transform')
var Constants = require('../../constants')

/**
 * This transform ensures that no requests will be sent before an /init or /start request.
 * As these are the two possible first requests that the API expects for a view.
 *
 * @constructs
 * @extends youbora.Transform
 * @memberof youbora
 * @name FlowTransform
 *
 * @param {Plugin} plugin Instance of {@link Plugin}
 */
var FlowTransform = Transform.extend(
  /** @lends youbora.FlowTransform.prototype */
  {
    _services: [Constants.Service.INIT, Constants.Service.START],

    /**
     * Returns if transform is blocking.
     *
     * @param {YBRequest} request Request to transform.
     * @return {bool} True if queue shall be blocked.
     */
    isBlocking: function (request) {
      if (this._isBusy && request != null) {
        if (this._services.indexOf(request.service) !== -1) {
          this.done()
        } else if (request.service === Constants.Service.ERROR) {
          return false
        }
      }

      return Transform.prototype.isBlocking.apply(this, arguments)
    }
  }
)

module.exports = FlowTransform
