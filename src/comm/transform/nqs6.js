var Transform = require('./transform')
var Constants = require('../../constants')

var Nqs6Transform = Transform.extend(
  /** @lends youbora.Nqs6Transform.prototype */
  {
    /**
     * This class ensures NQS6 backwards compatibility.
     * Deprecated since NQS6 clusters are essentialy gone.
     *
     * @deprecated
     * @constructs
     * @extends youbora.Transform
     * @memberof youbora
     */
    constructor: function () {
      this.done()
    },

    /**
     * Transform requests
     * @param {YBRequest} request YBRequest to transform.
     */
    parse: function (request) {
      this._cloneParam(request, 'accountCode', 'system')
      this._cloneParam(request, 'transactionCode', 'transcode')
      this._cloneParam(request, 'username', 'user')
      this._cloneParam(request, 'mediaResource', 'resource')
      this._cloneParam(request, 'msg', 'msg')

      if (request.service !== Constants.Service.JOIN) {
        this._cloneParam(request, 'playhead', 'time')
      }

      switch (request.service) {
        case Constants.Service.START:
          this._cloneParam(request, 'mediaDuration', 'duration')
          break

        case Constants.Service.JOIN:
          this._cloneParam(request, 'joinDuration', 'time')
          this._cloneParam(request, 'playhead', 'eventTime')
          break

        case Constants.Service.SEEK:
          this._cloneParam(request, 'seekDuration', 'duration')
          break

        case Constants.Service.BUFFER:
          this._cloneParam(request, 'bufferDuration', 'duration')
          break

        case Constants.Service.PING:
          for (var key in request.params.entities) {
            request.params.entityType = key
            request.params.entityValue = request.params.entities[key]
            break
          }
          break
      }
    },

    _cloneParam: function (request, from, to) {
      request.params[to] = request.params[from]
    }
  })

module.exports = Nqs6Transform
