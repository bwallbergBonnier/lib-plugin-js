var YBRequest = require('../request')
var Transform = require('./transform')
var Log = require('../../log')
var Util = require('../../util')
var Constants = require('../../constants')

var ViewTransform = Transform.extend(
  /** @lends youbora.ViewTransform.prototype */
  {
    /**
     * This class manages Fastdata service and view index.
     *
     * @constructs
     * @extends youbora.Transform
     * @memberof youbora
     *
     * @param {Plugin} plugin Instance of {@link Plugin}
     * @param {string} session If provided, plugin will use this as a FD response.
     */
    constructor: function (plugin, session) {
      Transform.prototype.constructor.apply(this, arguments)

      /** /data response */
      this.response = {}

      this._viewIndex = -1

      this._session = session

      this._httpSecure = plugin.options.httpSecure

      this._plugin = plugin
    },

    /**
     * Starts the 'FastData' fetching. This will send the initial request to YOUBORA in order to get
     * the needed info for the rest of the requests.
     *
     * This is an asynchronous process.
     *
     * When the fetch is complete, {@link #fastDataConfig} will contain the parsed info.
     * @see FastDataConfig
     */
    init: function () {
      var service = Constants.Service.DATA
      var params = {
        apiVersion: 'v7',
        outputformat: 'json'
      }

      params = this._plugin.requestBuilder.buildParams(params, service)
      if (params !== null) {
        Log.notice(service + ' ' + params.system)
        if (params.system === 'nicetest') {
          // "nicetest" is the default accountCode.
          // If found here, it's very likely that the customer has forgotten to set it.
          Log.error(
            'No accountCode has been set. Please set your accountCode inside plugin\'s options.'
          )
        }

        new YBRequest(this._plugin.getHost(), service, params)
          .on(YBRequest.Event.SUCCESS, this._receiveData.bind(this))
          .on(YBRequest.Event.ERROR, this._failedData.bind(this))
          .send()
      }
    },

    /**
     * Uses given response to set fastdata response.
     *
     * @param {String} response Fastdata response as json string.
     */
    setData: function (response) {
      try {
        var resp = JSON.parse(response)
        this.response.msg = response
        this.response.host = Util.addProtocol(resp.q.h, this._httpSecure)
        this.response.code = resp.q.c
        this.response.pingTime = resp.q.pt || 5
        this.response.beatTime = resp.q.bt || 30

        this.done()
      } catch (err) {
        Log.error('Fastdata response is invalid.')
      }
    },

    /**
     * Parse the response from the fastData service.
     *
     * @private
     */
    _receiveData: function (req, e) {
      var msg = req.getXHR().response
      this.setData(msg)
    },

    _failedData: function (req, e) {
      Log.error('Fastdata request has failed.')
    },

    /**
     * This method will increment the view index (_0, _1, _2...). The library handles this
     * automatically, but some error flow might need calling this manually.
     * @return {string} new viewcode
     */
    nextView: function () {
      this._viewIndex += 1
      return this.getViewCode()
    },

    /**
     * Returns current viewcode
     * @return {string} viewcode
     */
    getViewCode: function () {
      return this.response.code + '_' + this._viewIndex
    },

    /**
     * Returns the current sessionId
     *
     * @returns {string} SessionId
     */
    getSessionId: function () {
      return this._session
    },

    /**
     * Sets the sessionId
     *
     * @param {String} sessionId Sets the session id.
     */
    setSessionId: function (sessionId) {
      this._session = sessionId
    },

    /**
     * Transform requests
     * @param {youbora.comm.YBRequest} request YBRequest to transform.
     */
    parse: function (request) {
      request.host = request.host || this.response.host
      request.params.sessionId = request.params.sessionId || this.getSessionId()
      if (request.service === Constants.Service.PING ||
        request.service === Constants.Service.START) {
        request.params.pingTime = request.params.pingTime || this.response.pingTime
      }

      if ([ // Unless infinity event
        Constants.Service.NAV,
        Constants.Service.SESSION_START,
        Constants.Service.SESSION_STOP,
        Constants.Service.EVENT
      ].indexOf(request.service) === -1) {
        request.params.code = request.params.code || this.getViewCode()
      }
    }
  })

module.exports = ViewTransform
