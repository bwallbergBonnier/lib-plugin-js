// {@see CdnParser}

module.exports = {
  cdnName: 'AKAMAI',
  parsers: [{
    element: 'type+host',
    headerName: 'X-Cache',
    regex: /(.+)\sfrom\s.+\(.+\/(.+)\).*/
  }],
  parseType: function (type) {
    switch (type) {
      case 'TCP_HIT':
        return 1
      case 'TCP_MISS':
        return 2
      default:
        return 0
    }
  }
}
