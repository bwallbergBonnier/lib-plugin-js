var YBRequest = require('../../request')
var Emitter = require('../../../emitter')
var Log = require('../../../log')

var HlsParser = Emitter.extend(
  /** @lends youbora.HlsParser.prototype */
  {
    /**
     * Class that asynchronously parses an HLS resource in order to get to the transportstream URL.
     *
     * The point behind this class is that some customers do not host the HLS manifest in the same
     * host or even CDN where the actual content chunks are located.
     *
     * Since the CDN detection is performed with the resource url, it is essential that this
     * resource url is pointing to the CDN that is actually hosting the chunks.
     *
     * HLS manifests can be multi-level so this class uses a recursive approach to get to the final
     * chunk file.
     *
     * @constructs HlsParser
     * @extends youbora.Emitter
     * @memberof youbora
     */
    constructor: function () {
      this._realResource = null
    },

    /**
     * Emits DONE event
     */
    done: function () {
      this.emit(HlsParser.Event.DONE)
    },

    /**
     * Starts the HLS parsing from the given resource. The first (outside) call should set the
     * parentResource to null.
     *
     * @param {string} resource Either the resource url or the manifest body.
     * @param {string} parentResource Parent resource in case relative paths are sent.
     */
    parse: function (resource, parentResource) {
      parentResource = parentResource || ''

      try {
        var matches = /((^[^#]*?)(\.m3u8|\.m3u|\.ts|\.mp4)((\?|\;)\S*|\n|\r|$))/im.exec(resource)
      } catch (err) {
        Log.warn('Parse HLS Regex failed', err)
        this.done()
      }

      if (matches !== null) { // get first line ending in .m3u8, .m3u, .mp4 or .ts
        var res = matches[1].trim()

        var index = parentResource.lastIndexOf('/')
        if (res.indexOf('http') !== 0 && index !== -1) {
          // If does not start with http, add parentResource relative route.
          res = parentResource.slice(0, index) + '/' + res
        }

        if (matches[3] === '.m3u8' || matches[3] === '.m3u') { // It is m3u8 or m3u...
          var request = new YBRequest(res, null, null, { cache: true })

          request.on(YBRequest.Event.SUCCESS, function (resp) {
            this.parse(resp.getXHR().responseText, matches[2])
          }.bind(this))

          request.on(YBRequest.Event.ERROR, function (resp) {
            this.done()
          }.bind(this))

          request.send()
        } else { // It is mp4 or ts...
          this._realResource = res
          this.done()
        }
      } else {
        this.done()
      }
    },

    /**
     * Get the parsed resource. Will be null/undefined if parsing is not yet started and can be a partial
     * (an intermediate manifest) result if the parser is still running.
     *
     * @return {string} The parsed resource.
     */
    getResource: function () {
      return this._realResource
    }
  },

  /** @lends youbora.HlsParser */
  {
    // Static members

    /**
     * List of events that could be fired from this class.
     * @enum
     */
    Event: {
      /** Notifies that this HlsParser is done processing. */
      DONE: 'done'
    }
  }
)

module.exports = HlsParser
