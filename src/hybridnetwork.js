/**
 * This static class provides p2p and cdn network traffic information for
 * Streamroot, Peer5 and EasyBroadcast
 *
 * @class
 * @static
 * @memberof youbora
 */
var HybridNetowrk = {
  /** Returns CDN traffic bytes using streamroot or peer5. Otherwise null */
  getCdnTraffic: function () {
    if (typeof Streamroot !== 'undefined' && Streamroot.p2pAvailable && Streamroot.peerAgents) {
      var acum = 0
      for (agent in Streamroot.peerAgents) {
        acum += Streamroot.peerAgents[agent].stats.cdn
      }
      return acum
    }
    if (typeof peer5 !== 'undefined' && peer5.getStats) {
      return peer5.getStats().totalHttpDownloaded
    }
    return null
  },

  /** Returns P2P traffic bytes using streamroot or peer5. Otherwise null */
  getP2PTraffic: function () {
    if (typeof Streamroot !== 'undefined' && Streamroot.p2pAvailable && Streamroot.peerAgents) {
      var acum = 0
      for (agent in Streamroot.peerAgents) {
        if (Streamroot.peerAgents[agent].isP2PEnabled)
          acum += Streamroot.peerAgents[agent].stats.p2p
      }
      return acum
    }
    if (typeof peer5 !== 'undefined' && peer5.getStats) {
      return peer5.getStats().totalP2PDownloaded
    }
    return null
  },

  /** Returns P2P traffic sent in bytes, using streamroot or peer5. Otherwise null*/
  getUploadTraffic: function () {
    if (typeof Streamroot !== 'undefined' && Streamroot.p2pAvailable && Streamroot.peerAgents) {
      var acum = 0
      for (agent in Streamroot.peerAgents) {
        if (Streamroot.peerAgents[agent].isP2PEnabled)
          acum += Streamroot.peerAgents[agent].stats.upload
      }
      return acum
    }
    if (typeof peer5 !== 'undefined' && peer5.getStats) {
      return peer5.getStats().totalP2PUploaded
    }
    return null
  },


  /** Returns if P2P is enabled, using streamroot or peer5. Otherwise null*/
  getIsP2PEnabled: function () {
    if (typeof Streamroot !== 'undefined' && Streamroot.p2pAvailable && Streamroot.peerAgents) {
      var acum = false
      for (agent in Streamroot.peerAgents) { // if at least one agent is enabled
        acum = acum || Streamroot.peerAgents[agent].isP2PEnabled
      }
      return acum
    }
    if (typeof peer5 !== 'undefined' && peer5.isEnabled) {
      return peer5.isEnabled()
    }
    return null
  }
}

module.exports = HybridNetowrk
